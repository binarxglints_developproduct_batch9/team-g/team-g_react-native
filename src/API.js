export const API_USER = 'https://protra.kuyrek.com/user/';
export const API_REGISTER = 'https://protra.kuyrek.com/user/signup';
export const API_LOGIN = 'https://protra.kuyrek.com/user/login';
export const API_PROJECT = 'https://protra.kuyrek.com/project/';
export const API_TASKS = 'https://protra.kuyrek.com/task/';
export const API_ACTIVITY = 'https://protra.kuyrek.com/activity/';
