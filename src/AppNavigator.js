import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import RootStack from './RootStackNavigator';
import messaging from '@react-native-firebase/messaging';

const AppNavigator = () => {
  const unsubscribe = async () => {
    await messaging().getToken();
    await messaging().subscribeToTopic('sendNotification');
    messaging().onMessage(async mess => {
      console.log(mess, 'message');
    });
  };
  useEffect(() => {
    unsubscribe();
  }, []);

  return (
    <NavigationContainer>
      <RootStack />
    </NavigationContainer>
  );
};

export default AppNavigator;
