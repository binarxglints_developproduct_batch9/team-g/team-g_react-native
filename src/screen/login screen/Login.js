import React, { useState, useEffect } from 'react';
import { View, StyleSheet, StatusBar, Text, SafeAreaView } from 'react-native';
import { Image, Icon, Button } from 'react-native-elements';
import {
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import AnimatedLoader from "react-native-animated-loader";
import Spinner from 'react-native-loading-spinner-overlay';
import { useDispatch } from 'react-redux';
//mport LottieAnimation from 'easy-lottie-react-native';
import LottieView from 'lottie-react-native';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';

import { LinearConfig } from '../../LinearConfig';
import Protra from '../../asset/Protra.png';
import { API_LOGIN, API_USER } from '../../API';
import { ISLOGIN } from '../../redux/Reducer/case';
import Animated from 'react-native-reanimated';

const Login = () => {
  const navigation = useNavigation();
  const [splash, setSplash] = useState(true);
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [alert, setAlert] = useState(false);
  const [loading, setLoading] = useState(false);
  const [hidden, setHidden] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    GoogleSignin.configure()
      // offlineAccess: true,
      //   webClientId:
      //     '77969533754-cbdim87ah7pql83isdof67v5bu3udguq.apps.googleusercontent.com',
  },[])
 
  const onChangeEmail = val => {
    setEmail(val);
  };

  const onChangePassword = val => {
    setPassword(val);
  };

  const handleLogin = async () => {
    setLoading(true);
    try {
      const res = await Axios.post(API_LOGIN, {
        email: email,
        password: password,
      });

      if (res !== null) {
        const status = res.data.status;
        const data = res.data;
        if (status == 'success') {
          await AsyncStorage.setItem('userToken', data.token);
          setLoading(false);
          dispatch({ type: ISLOGIN });
          navigation.navigate('Dashboard');
          navigation.reset({
            index: 0,
            routes: [{ name: 'Dashboard' }],
          });
        } else {
          console.log('error');
          setLoading(false);
          setAlert(true);
        }
      } else {
        console.log('Login Failed');
        setAlert(true);
        setLoading(false);
      }
    } catch (error) {
      console.log(error, 'error');
      setLoading(false);
      setAlert(true);
    }
  };

  const googleLogin = async () => {
    try {

      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      if (userInfo !== null) {
        const token = await GoogleSignin.getTokens();
        try {
          console.log("Ini log  " , token)
          console.log("Ini log user " , userInfo)
          const res = await Axios.post(`${API_USER}/verify`, {
            // name: userInfo.user.name,
            // access_token: token.accessToken
            token : token.accessToken
          });
          if (res !== null) {
            await AsyncStorage.setItem('userToken', token);
            navigation.navigate('Dashboard');
          }
        } catch (error) {
          console.log(error, 'error');
        }
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('error occured SIGN_IN_CANCELLED');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('error occured IN_PROGRESS');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('error occured PLAY_SERVICES_NOT_AVAILABLE');
      } else {
        console.log(error);
        console.log('error occured unknow error');
      }
    }
  };
  const handleFinishAnimation = () => {
    let routes;
    if (login === true) {
      routes = 'Dashboard';
    } else {
      routes = 'Landing Page';
    }
    navigation.navigate(routes);
    navigation.reset({
      index: 0,
      routes: [{ name: routes }],
    });
  };
  return (


    <View style={styles.container}>
      <StatusBar backgroundColor={"#FFF"} barStyle='dark-content' translucent={false} />
      <SafeAreaView style={{ flex: 1 }}>
        <Image source={Protra} style={styles.logo} />
        <Text style={styles.inputText}>Email</Text>
        <TextInput style={styles.inputBox} onChangeText={onChangeEmail} />
        <Text style={styles.inputText}>Password</Text>
        <TextInput
          secureTextEntry={hidden}
          style={styles.inputBox}
          onChangeText={onChangePassword}
        />
        <Icon
          type="feather"
          name={hidden ? 'eye-off' : 'eye'}
          size={25}
          containerStyle={styles.icon}
          onPress={() => setHidden(!hidden)}
        />
        <View>
          <Button
            buttonStyle={styles.loginbutton}
            title="Log In"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleLogin}
          />
        </View>
        {/* <GoogleSigninButton
          onPress={googleLogin}
          style={styles.buttonGoogle}
        /> */}
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate('Signup')}>
          <Text style={styles.signup}>Register ></Text>
        </TouchableWithoutFeedback>
        <AwesomeAlert
          show={alert}
          showProgress={false}
          title="Oops"
          message="Wrong Email or Password"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={true}
          showConfirmButton={true}
          confirmText="Oke"
          confirmButtonColor="#DD6B55"
          onConfirmPressed={() => {
            setAlert(false);
          }}
        />
        {/* <LottieAnimation 
        visible={loading}
        source={require('../../asset/2-loader.json')} /> */}

        {/* <AnimatedLoader
          visible={loading}
          overlayColor="rgba(255,255,255,0.75)"
          source={require("../../asset/2-loader.json")}
          animationStyle={styles.lottie}
          speed={1}
        >
        </AnimatedLoader> */}
        <Spinner visible={loading}
        // size="small"
        >
          {/* <AnimatedLoader
            animationStyle={styles.lottie}
            source={require('../../asset/2-loader.json')}
            loop={splash}
            // autoPlay={true}
            speed={0.75}
          // resizeMode='center'
          /> */}
        </Spinner>
      </SafeAreaView>
    </View>

  );
};
export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 80,
  },
  logo: {
    height: 13,
    width: 83,
    marginTop: 109,
    marginBottom: 104,
    marginHorizontal: 143,
    alignItems: 'center'
  },
  inputContainer: {
    marginHorizontal: 16,
  },
  inputBar: {
    justifyContent: 'flex-start',
    marginVertical: 12,
  },
  inputText: {
    height: 30,
    width: 100,
    marginHorizontal: 16,
    marginBottom: 8,
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#000'
  },
  inputBox: {
    height: 60,
    width: 360,
    paddingLeft: 15,
    marginHorizontal: 16,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#C4C4C4',
    backgroundColor: '#FAFAFA',
    borderWidth: 1,
    borderRadius: 3,
    fontFamily: 'Montserrat',
  },
  button: {
    marginVertical: 13,
    marginHorizontal: 16,
    backgroundColor: '#FFF',
    alignSelf: 'center'
  },
  buttonGoogle: {
    height: 50,
    width: 50,
    marginVertical: 13,
    marginHorizontal: 16,
    borderRadius: 30,
    backgroundColor: '#FFF',
    alignSelf: 'center'
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    height: 23,
    width: 65,
    marginHorizontal: 16,
    paddingHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fff'
  },
  signup: {
    alignSelf: 'flex-end',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 16,
  },
  buttonContainer: {
    marginVertical: 45,
    paddingBottom: 95,
  },
  icon: {
    alignSelf: 'flex-end',
    bottom: 53,
    right: 30,
  },
  loginText: {
    fontFamily: 'Montserrat',
    textAlign: 'center',
    marginTop: 3,
  },
  loginbutton: {
    height: 60,
    width: 303,
    marginTop: 3,
    marginBottom: 5,
    marginHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lottie: {
    width: 10,
    height: 10
  }
});
