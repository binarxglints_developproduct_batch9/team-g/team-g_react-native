import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, StatusBar } from 'react-native';
import {
  TextInput,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import { Image, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-picker';
import { getProjectDetail, editProject } from '../../redux/Action/ProjectAction';
import { GET_PROJECT_TITLE } from '../../redux/Case Type/Project';
import { LinearConfig } from '../../LinearConfig';

const EditProject = () => {
  const navigation = useNavigation();
  const [title, setTitle] = useState('');
  const [descrip, setDescrip] = useState('');
  const dispatch = useDispatch();
  const [filePath, setFilePath] = useState('');
  const loading = useSelector(state => state.project.isLoading);
  const project = useSelector(state => state.project);
  const idproject = useSelector(state => state.project.idProject);
  console.log("ini id Project " + idproject)
  
  const projectTitle = project.name;
  const projectDescrip = project.description;
  const projectImage = project.bgImage;

  const onChangeTitle = val => {
    setTitle(val);
  };

  const onChangeDescrip = val => {
    setDescrip(val);
  };

  useEffect(() => {
    dispatch(getProjectDetail(project._id));
  }, [dispatch, project._id]);

  const ChooseFile = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setFilePath(response);
      }
    });
  };

  const handleEditProject = () => {
    let dataForm = new FormData();
    if (title !== '') {
      dataForm.append('name', title);
      dispatch({ type: GET_PROJECT_TITLE, name: title });
    }
    if (descrip !== '') {
      dataForm.append('description', descrip);
    }
    if (filePath !== '') {
      dataForm.append('bgImage', {
        name: filePath.fileName,
        type: filePath.type,
        uri: filePath.uri,
      });
    }
    dispatch(editProject(idproject, dataForm, navigation));
  };

  let fileImage;
  if (filePath !== '') {
    fileImage = { uri: filePath.uri };
  } else {
    fileImage = { uri: projectImage };
  }
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <ScrollView>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Title</Text>
          <TextInput
            placeholder="Title"
            style={styles.titleInput}
            onChangeText={onChangeTitle}
            defaultValue={projectTitle}
          />
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.title}>Description</Text>
          <TextInput
            placeholder="Description"
            multiline={true}
            style={styles.descriptionInput}
            onChangeText={onChangeDescrip}
            defaultValue={projectDescrip}
          />
        </View>
        <View style={styles.imageContainer}>
          <Text style={styles.title}>Project Image</Text>
          <TouchableWithoutFeedback onPress={ChooseFile}>
            <Image source={fileImage} containerStyle={styles.image} />
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
      {/* <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={handleEditProject}>
              <LinearGradient
              colors={['#2DC0CA','#1589CB']}
              style={styles.button}>
                <Text style={styles.buttonTitle}>Save Changes</Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <LinearGradient
              colors={['#FFF','#FAFAFA']}
              style={styles.cancelButton}>
                <Text style={styles.cancelTitle}>Cancel</Text>
              </LinearGradient>
            </TouchableOpacity>
        </View> */}
      <View>
        <Button
          buttonStyle={styles.saveButton}
          title="Save Changes"
          containerStyle={styles.button}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={styles.buttonTitle}
          onPress={handleEditProject}
        />
      </View>
      <View>
        <Button
          buttonStyle={styles.cancelButton}
          title="Cancel"
          containerStyle={styles.Button}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={styles.cancelTitle}
          onPress={() => navigation.goBack()}
        />
      </View>
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default EditProject;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  titleContainer: {
    marginHorizontal: 16,
    marginTop: 28,
  },
  title: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  titleInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    height: 100,
    textAlignVertical: 'top',
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 8,
  },
  imageContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  button: {
    marginHorizontal: 16,
    alignSelf: 'center'
  },
  saveButton: {
    height: 60,
    width: 303,
    marginTop: 3,
    marginBottom: 5,
    marginHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    height: 23,
    width: 303,
    marginHorizontal: 16,
    paddingHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fff'
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    height: 23,
    width: 303,
    marginHorizontal: 16,
    paddingHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fff'
  },
  cancelButton: {
    height: 60,
    width: 303,
    marginTop: 3,
    marginBottom: 5,
    marginHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Button: {
    marginHorizontal: 16,
    alignSelf: 'center'
  },
  buttonContainer: {
    marginTop: 50,
  },
});
