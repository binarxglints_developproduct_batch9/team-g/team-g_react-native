import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, StatusBar } from 'react-native';
import {
  TextInput,
  ScrollView,
} from 'react-native-gesture-handler';
import { Image, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-picker';

import addImage from '../../asset/addImage.png';
import { createProject } from '../../redux/Action/ProjectAction';
import { STATUS } from '../../redux/Case Type/Project';
import { LinearConfig } from '../../LinearConfig';


const CreateProject = () => {
  const navigation = useNavigation();
  const [title, setTitle] = useState('');
  const [descrip, setDescrip] = useState('');
  const dispatch = useDispatch();
  const [filePath, setFilePath] = useState('');
  const loading = useSelector(state => state.project.isLoading);

  const onChangeTitle = val => {
    setTitle(val);
  };

  const onChangeDescrip = val => {
    setDescrip(val);
  };

  const ChooseFile = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setFilePath(response);
      }
    });
  };

  const handleCreateProject = () => {
    let dataForm = new FormData();
    dataForm.append('name', title);
    dataForm.append('description', descrip);
    if (filePath !== '') {
      dataForm.append('bgImage', {
        name: filePath.fileName,
        type: filePath.type,
        uri: filePath.uri,
      });
    }
    dispatch(createProject(dataForm, navigation));
  };
  let fileImage;
  if (filePath !== '') {
    fileImage = { uri: filePath.uri };
  } else {
    fileImage = addImage;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <Text style={styles.title}>Title</Text>
      <TextInput
        placeholder="Title"
        style={styles.titleInput}
        onChangeText={onChangeTitle}
        maxLength={40}
      />
      <Text style={styles.title}>Description</Text>
      <TextInput
        placeholder="Description"
        multiline={true}
        style={styles.descriptionInput}
        onChangeText={onChangeDescrip}
      />
      <View>
        <Text style={styles.title}>Project Image</Text>
        <ScrollView horizontal>
          <TouchableOpacity onPress={ChooseFile}>
            <Image source={fileImage} style={styles.image} />
          </TouchableOpacity>
        </ScrollView>
      </View>

      {/* <TouchableOpacity onPress={handleCreateProject}>
          <LinearGradient
            colors={['#2DC0CA', '#1589CB']}
            style={styles.button}>
            <Text style={styles.createTitle}>Create Project</Text>
          </LinearGradient>
        </TouchableOpacity> */}
      <View>
        <Button
          buttonStyle={styles.Cbutton}
          title="Create Project"
          containerStyle={styles.button}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={styles.buttonTitle}
          onPress={handleCreateProject}
        />
      </View>
      {/* <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <LinearGradient
            colors={['#FFF', '#FAFAFA']}
            style={styles.buttonCancel}>
            <Text style={styles.cancelTitle}>Cancel</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View> */}
      <View>
        <Button
          buttonStyle={styles.Cbutton}
          title="Cancel"
          containerStyle={styles.buttonCancel}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={styles.buttonTitle}
          onPress={() => navigation.goBack()}
        />
      </View>
      <Spinner visible={loading} size="large" />
    </View>
  )
};

export default CreateProject;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  titleContainer: {
    marginHorizontal: 16,
    marginTop: 28,
  },
  title: {
    height: 30,
    width: 358,
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 10,
    marginLeft: 16
  },
  titleInput: {
    height: 60,
    width: 358,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    marginLeft: 16,
    paddingHorizontal: 14,
    marginTop: 8,
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionInput: {
    height: 60,
    width: 358,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginLeft: 16,
    marginTop: 3,
    height: 100,
    textAlignVertical: 'top',
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 8,
    marginBottom: 60,
    marginLeft: 16
  },
  imageContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  button: {
    marginVertical: 13,
    marginHorizontal: 16,
    backgroundColor: '#FFF',
    alignSelf: 'center'
  },
  buttonCancel: {
    marginHorizontal: 16,
    backgroundColor: '#FFF',
    alignSelf: 'center'
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  createTitle: {
    fontFamily: 'Montserrat',
    height: 23,
    width: 303,
    marginTop: 15,
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fff'
  },
  cancelTitle: {
    height: 23,
    width: 303,
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    marginTop: 15,
    textAlign: 'center',
    color: '#1589CB',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
  buttonContainer: {
    marginTop: 50,
  },
  Cbutton: {
    height: 60,
    width: 303,
    marginTop: 3,
    marginBottom: 5,
    marginHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50
  },
});
