import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import AwesomeAlert from 'react-native-awesome-alerts';
import { TextInput, ScrollView } from 'react-native-gesture-handler';
import { Button, Icon } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import { LinearConfig } from '../../LinearConfig';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import { API_USER } from '../../API';

const UpdatePassword = () => {
  const navigation = useNavigation();
  const [newPassword, setnewPassword] = useState('');
  // const [cPassword, setCPassword] = useState('');
  const [reEnterPassword, setreEnterPassword] = useState('');
  const [oldPassword, setoldPassword] = useState('');
  const [alert, setAlert] = useState(false);
  const [hidden, setHidden] = useState(true);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const [success, setSuccess] = useState(false);

  const onChangePassword = val => {
    setnewPassword(val);
  };

  // const onChangeCPassword = val => {
  //   setCPassword(val);
  // };
  const onChangeCPassword = val => {
    setreEnterPassword(val);
  };

  const onChangeoldPassword = val => {
    setoldPassword(val);
  };

  const handleUpdatePassword = async () => {
    // const navigation = useNavigation();

    let dataF = new Data();
    // if (Password === cPassword) {
    if (oldPassword !== '') {
      dataForm.append('oldPassword', oldPassword);
      dispatch({ type: GET_OLD_PASSWORD, oldPassword: oldPassword });
    }
    if (newPassword !== '') {
      dataForm.append('newPassword', newPassword);
      dispatch({ type: GET_NEW_PASSWORD, newPassword: newPassword });
    }
    if (reEnterPassword !== '') {
      dataForm.append('reEnterPassword', reEnterPassword);
      dispatch({ type: GET_OLD_PASSWORD, reEnterPassword: reEnterPassword });
    }
    setLoading(true);
    try {
      const token = await AsyncStorage.getItem('userToken');
      // const res = await
      // await Axios.put(
      // `${API_USER}/change_password`,
      Axios({
        method: 'PUT',
        url: `${API_USER}/change_password`,
        data: dataF,
        headers: {
          Authorization: `bearer ${token}`
        },
      })
        .then(({ data }) => {

            if (data !== null) {
          console.log("ini log data " + data)
          const status = data.status
          if (status == 'success') {
            setLoading(false);
            navigation.navigate('Edit Profile');
            setSuccess(true);
          } else {
            setLoading(false);
          }
        }
        });
      // }})
    } catch (error) {
      console.log(error, 'error');
      setLoading(false);
      setMessage(error.message);
      setAlert(true);
    }
    // } 
    // else {
    //   setMessage('Password not match');
    //   setAlert(true);
    //   setLoading(false);
    // }
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.inputContainer}>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Old Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangeoldPassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>New Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangePassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
            <Text style={{ fontFamily: 'Montserrat', fontSize: 10 }}>
              Password minimum eight characters, contain uppercase, lowercase
              or number
            </Text>
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Confirm New Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangeCPassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title="Change Password"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleUpdatePassword}
          />
          <Button
            title="Cancel"
            containerStyle={styles.button}
            titleStyle={styles.cancelTitle}
            buttonStyle={styles.cancelButton}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
      <AwesomeAlert
        show={alert}
        showProgress={false}
        title="Oops"
        message={message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="Ok"
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setAlert(false);
        }}
      />
      <AwesomeAlert
        show={success}
        showProgress={false}
        title="Note"
        message="Password changed"
        showConfirmButton={true}
        confirmText="Protra"
        confirmButtonColor="#25ed21"
        onConfirmPressed={() => {
          navigation.navigate('Profile');
          setSuccess(false);
        }}
      />
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default UpdatePassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    justifyContent: 'flex-start',
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  logo: {
    width: 15,
    height: 15,
    marginHorizontal: 5,
  },
  logoText: {
    fontFamily: 'Metropolis',
    fontWeight: 'bold',
    fontSize: 18,
    letterSpacing: -0.7,
  },
  inputContainer: {
    marginHorizontal: 16,
  },
  inputBar: {
    justifyContent: 'flex-start',
    marginVertical: 10,
  },
  inputText: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  inputBox: {
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    backgroundColor: '#FFF',
    marginTop: 8,
    paddingHorizontal: 14,
  },
  button: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 50,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  signup: {
    alignSelf: 'flex-end',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 16,
  },
  icon: {
    alignSelf: 'flex-end',
    position: 'absolute',
    top: 45,
    right: 15,
  },
  warning: {
    color: '#ff0d00',
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
});
