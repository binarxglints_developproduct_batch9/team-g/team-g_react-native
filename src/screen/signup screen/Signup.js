import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Image, Button, Icon} from 'react-native-elements';
import Logo from '../../asset/Protra.png';
import {
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import {API_REGISTER} from '../../API';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import {useDispatch} from 'react-redux';
import {ISLOGIN} from '../../redux/Reducer/case';

const Signup = () => {
  const navigation = useNavigation();
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [cPassword, setCPassword] = useState('');
  const [alert, setAlert] = useState(false);
  const [hidden, setHidden] = useState(true);
  const [loading, setLoading] = useState(false);
  const [valid, setValid] = useState(false);
  const dispatch = useDispatch();
  const [message, setMessage] = useState('');

  const onChangePassword = val => {
    setPassword(val);
  };

  const onChangeCPassword = val => {
    setCPassword(val);
  };

  const onChangeUsername = val => {
    setUsername(val);
  };

  const handleEmail = val => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(val) === false) {
      setValid(true);
      setEmail(val);
      return false;
    } else {
      setValid(false);
      setEmail(val);
      console.log('Email is correct');
    }
  };

  const handleRegister = async () => {
    setLoading(true);
    if (password === cPassword) {
      try {
        const res = await Axios.post(API_REGISTER, {
          fullName: username,
          email: email,
          password: password,
          passwordConfirmation: password,
        });

        if (res !== null) {
          const status = res.data.status;
          if (status == 'success') {
            setLoading(false);
            dispatch({type: ISLOGIN});
            navigation.navigate('Verify');
          }
        } else {
          console.log('register failed');
          setLoading(false);
        }
      } catch (error) {
        if (
          error.response.status === 422 &&
          error.response.data.message ==
            'Validation error: Email already exist. Use another one or login'
        ) {
          setMessage('Email already exist');
          setAlert(true);
          setLoading(false);
        } else if (
          error.response.status === 422 &&
          error.response.data.message ==
            'Validation error: Password minimum eight characters, contain uppercase, lowercase and number'
        ) {
          setMessage(
            'Password must be minimum eight characters, contain uppercase, lowercase and number',
          );
          setAlert(true);
          setLoading(false);
        } else if (
          error.response.status === 422 &&
          error.response.data.message ==
            'Validation error: Password minimum eight characters, contain uppercase, lowercase and number,\nValidation error: Email already exist. Use another one or login'
        ) {
          setMessage('Email already exis');
          setAlert(true);
          setLoading(false);
        } else {
          setMessage('E-mail has been registered');
          setAlert(true);
          setLoading(false);
        }
      }
    } else {
      setMessage('Password not match');
      setAlert(true);
      setLoading(false);
    }
  };

  let invalid;
  if (valid === true) {
    invalid = <Text style={styles.warning}>*Invalid Email Format</Text>;
  }

  return (
    <View style={styles.container}>
      <Image source={Logo} style={styles.logo}/>
        <View style={styles.inputContainer}>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Full Name</Text>
            <TextInput
              style={styles.inputBox}
              onChangeText={onChangeUsername}
            />
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Email</Text>
            <TextInput style={styles.inputBox} onChangeText={handleEmail} />
            {invalid}
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangePassword}
            />
            <Icon
              type="feather"
              name={hidden ? "eye-off" : "eye"}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
            <Text style={{fontFamily: 'Montserrat', fontSize: 10}}>
              Password minimum eight characters, contain uppercase, lowercase
              or number
            </Text>
          </View>
          <View style={styles.inputBar}>
            <Text style={styles.inputText}>Confirm Password</Text>
            <TextInput
              secureTextEntry={hidden}
              style={styles.inputBox}
              onChangeText={onChangeCPassword}
            />
            <Icon
              type="feather"
              name={hidden ? 'eye-off' : 'eye'}
              size={25}
              containerStyle={styles.icon}
              onPress={() => setHidden(!hidden)}
            />
          </View>
        </View>
        <TouchableOpacity onPress={handleRegister}>
        <LinearGradient
        colors={['#2DC0CA','#1589CB']}
        style={styles.button}>
          <Text
          style={styles.TextButton}>Register
          </Text>
        </LinearGradient>
      </TouchableOpacity>
       
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('Login')}>
            <Text style={styles.signup}>Log In ></Text>
          </TouchableWithoutFeedback>
    
      <AwesomeAlert
        show={alert}
        showProgress={false}
        title="Oops"
        message={message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="Oke"
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          setAlert(false);
        }}
      />
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default Signup;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 30,
  },
  logo: {
    height: 13,
    width: 83,
    marginTop: 59,
    marginBottom: 60,
    marginHorizontal:143,
    alignItems: 'center'
  },
  logoText: {
    fontFamily: 'Metropolis',
    fontWeight: 'bold',
    fontSize: 18,
    letterSpacing: -0.7,
  },
  inputContainer: {
    marginHorizontal: 16,
  },
  inputBar: {
    justifyContent: 'flex-start',
    marginVertical: 10,
  },
  inputText: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  inputBox: {
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    backgroundColor: '#FFF',
    marginTop: 8,
    paddingHorizontal: 14,
  },
  button: {
    height:60, 
    width:360,
    marginTop:35,
    marginBottom: 5,
    marginHorizontal:16,
    justifyContent:'center',
    alignItems:'center',
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  signup: {
    alignSelf: 'flex-end',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 16,
  },
  icon: {
    alignSelf: 'flex-end',
    position: 'absolute',
    top: 45,
    right: 15,
  },
  warning: {
    color: '#ff0d00',
  },
  TextButton : {
    height: 23,
    width:75,
    marginHorizontal:16,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize:18,
    fontWeight: '700',
    color:'#fff'
  },
});
