import React, { useState, useEffect } from 'react';
import { StatusBar } from 'react-native';
import LottieView from 'lottie-react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch } from 'react-redux';

import { ISLOGIN } from '../../redux/Reducer/case';


const SplashScreen = () => {
  const [splash, setSplash] = useState(true);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [login, setLogin] = useState(false);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const getData = () => {
    try {
      AsyncStorage.getItem('userToken').then(token => {
        if (token) {
          dispatch({ type: ISLOGIN });
          setLogin(true);
          setSplash(false);
        } else {
          setSplash(false);
        }
      });
    } catch (error) {
      console.log('no token');
      setSplash(false);
    }
  };

  useEffect(() => {
    getData();
  }, [getData]);

  const handleFinishAnimation = () => {
    let routes;
    if (login === true) {
      routes = 'Dashboard';
    } else {
      routes = 'Landing Page';
    }
    navigation.navigate(routes);
    navigation.reset({
      index: 0,
      routes: [{ name: routes }],
    });
  };

  return (
    <>
      <LottieView
        source={require('../../asset/analytics.json')}
        autoPlay={true}
        loop={splash}
        onAnimationFinish={handleFinishAnimation}
        speed={0.75}
        resizeMode='contain'>
          <StatusBar hidden />
      </LottieView>
    </>
  );
};

export default SplashScreen;
