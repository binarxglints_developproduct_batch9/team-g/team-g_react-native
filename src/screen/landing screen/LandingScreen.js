import React from 'react';
import { View, StyleSheet, Text, StatusBar } from 'react-native';
import { Image, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { LinearConfig } from '../../LinearConfig';
import { useNavigation } from '@react-navigation/native';
import Swiper from 'react-native-swiper';
import Starter from '../../asset/ProTraico.png';


const LandingScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#FFF' barStyle="dark-content" />
      <Swiper loop={false}>
        <View style={styles.slide}>
          <Image source={Starter} style={styles.slide1Image} />
        </View>
      </Swiper>
      <View style={styles.buttonContainer}>
        <Button
          title="Log In"
          containerStyle={styles.button}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={styles.buttonTitle}
          onPress={() => navigation.navigate('Login')}
        />
        <Button
          title="Register"
          containerStyle={[styles.button]}
          titleStyle={[styles.buttonTitle, { color: '#1589CB' }]}
          type="outline"
          onPress={() => navigation.navigate('Signup')}
        />
      </View>
      <View style={styles.termText}>
        <Text style={{ textAlign: 'center' }}>
          <Text>By creating an account, you agree to our</Text>
          <Text style={{ fontWeight: 'bold' }}> Terms of Service</Text>
          <Text> and</Text>
          <Text style={{ fontWeight: 'bold' }}> Privacy Policy</Text>.
        </Text>
      </View>
    </View>
  );
};

export default LandingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    backgroundColor: '#F4F4F4',
    borderWidth: 1,
    borderColor: '#FAFAFA',
    marginBottom: 13,
    marginHorizontal: 16,
    borderRadius: 3,
    width: 120,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  termText: {
    fontFamily: 'Montserrat',
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 8,
    color: '#07689F',
    marginHorizontal: 16,
  },
  bodyText: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginBottom: 16,
    textAlign: 'center',
    color: '#07689F',
    marginHorizontal: 16,
  },
  slide: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide1Image: {
    width: 220,
    height: 220,
    marginTop: 116,
  },
});
