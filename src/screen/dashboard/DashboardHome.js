import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import { Header, Icon, Image } from 'react-native-elements';
import { TouchableNativeFeedback, FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import Avatar from '../../asset/avatar.png';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getAllProject } from '../../redux/Action/ProjectAction';
import { getTask } from '../../redux/Action/TaskAction';
import { GET_PROJECT_ID } from '../../redux/Case Type/Project';
import messaging from '@react-native-firebase/messaging';
import { getNews } from '../../redux/Action';
import moment from 'moment';
import { Menu } from 'react-native-paper';
import item from './project screen/DashboardProject';
import { GoogleSignin } from '@react-native-community/google-signin';

const MyLeftComponent = () => {
  return <Text style={styles.headerText}>Dashboard</Text>;
};
const MyRightComponent = props => {
  const [visible, setVisible] = useState(false);
  const navigation = useNavigation();
  const ProNavigate = () => {
    navigation.navigate('Profile');
    setVisible(false);
  };
  const handleLogout = async () => {
    try {
      AsyncStorage.clear();
      GoogleSignin.signOut();
      navigation.navigate('Landing Page');
      navigation.reset({
        index: 0,
        routes: [{ name: 'Landing Page' }],
      });
    } catch (error) {
      console.log(error, 'error');
    }
  }
  return (
    <View style={styles.icon}>
      <Menu
        visible={visible}
        anchor={
          <Icon
            type="feather"
            name="menu"
            size={25}
            onPress={() => setVisible(!visible)}
          />
        }
        onDismiss={() => setVisible(false)}>
        <Menu.Item title="Profile"
          style={styles.menuItemProf}
          onPress={ProNavigate} />
        <Menu.Item title="Log Out"
          style={styles.menuItemLog}
          onPress={handleLogout} />
      </Menu>
    </View>
  );
};
const DashboardHome = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const project = useSelector(state => state.project.project);
  const tasks = useSelector(state => state.project.countTask);
  const history = useSelector(state => state.showUser.history);
  const profile = useSelector(state => state.showUser);
  const image = profile.imageProfile;
  // const Members = useSelector(state => state.project.projectMember);


  useEffect(() => {
    dispatch(getAllProject());
  }, [dispatch, project]);

  useEffect(() => {
    dispatch(getTask());
  }, [dispatch, tasks]);

  useEffect(() => {
    dispatch(getNews());
  }, [dispatch]);

  // const handleDetailNavigation = _id => {
  //   dispatch({ type: GET_PROJECT_ID, idProject: _id });
  //   navigation.navigate('Project Detail');
  // };

  const renderItem = ({ item }) => (
    <View style={styles.projectBar}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <Text style={styles.projectBarTitle}>{item.name}</Text>
      <Image source={{ uri: image }} style={styles.imageProfile} />
      {/* <Text
        style={[styles.projectBarTitle, {marginRight: 5, color: '#7B7B7B'}]}>
        {item.type}
      </Text> */}
      {/* <Text
        style={[styles.projectBarDate, {marginRight: 4, color: '#7B7B7B'}]}>
        {item.action} at {moment(item.time).format('LLL')}
      </Text> */}
    </View>
  );

  return (
    <>
      <Header
        leftComponent={<MyLeftComponent />}
        rightComponent={<MyRightComponent />}
        backgroundColor="#FFF"
        containerStyle={styles.containerHeader}
      />
      <View style={styles.container}>
        <View style={styles.headerStatusContainer}>
          <View style={styles.headerStatus}>
            <Text style={styles.headerTextStatus}>{project.length}</Text>
            <Text style={styles.headerStatusTitle}>Total Project</Text>
          </View>
          <View style={styles.headerStatus}>
            <Text style={styles.headerTextStatus2}>
              {tasks !== null ? tasks : 0}
            </Text>
            <Text style={styles.headerTextStatus2}>{history}</Text>
            <Text style={styles.headerStatusTitle2}>Total Task</Text>
          </View>
        </View>
        <View style={styles.projectContainer}>
          <Text style={styles.projectTitle}>Latest Project</Text>
          <View style={styles.projectBarContainer}>
            <FlatList
              data={project}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              maxToRenderPerBatch={10}
            />
          </View>
        </View>
      </View>
    </>
  );
};

export default DashboardHome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginLeft: 16,
    marginTop: 15,
    color: '#000',
    width: 100,
  },
  icon: {
    marginTop: 15,
    marginRight: 10
  },
  menuItemProf: {
    height: 40,
    width: 40,
    marginTop: 10,
    marginRight: 10,
    paddingLeft: 5,
    // borderBottomWidth :1
  },
  menuItemLog: {
    height: 40,
    width: 40,
    marginTop: 0,
    marginRight: 10
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
  },
  headerStatus: {
    borderWidth: 1,
    borderColor: '#000',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#FFF',
    marginVertical: 4,
  },
  headerStatusContainer: {
    marginVertical: 30,
    marginHorizontal: 16,
  },
  headerTextStatus: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 36,
    marginHorizontal: 50,
  },
  headerTextStatus2: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 36,
    marginLeft:0,
  },
  headerStatusTitle: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginLeft: 20,
  },
  headerStatusTitle2: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginLeft: 80,
  },
  projectTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 24,
  },
  projectContainer: {
    marginHorizontal: 16,
  },
  Image: {
    width: 40,
    height: 40,
    borderRadius: 100,
  },
  projectBarImageContainer: {
    flexDirection: 'row',
    marginHorizontal: 14,
    marginVertical: 12,
  },
  projectBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#000',
    marginVertical: 4,
    backgroundColor: '#FFF',
    padding: 8,
  },
  projectBarTitle: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    alignSelf: 'flex-start',
    marginLeft: 16,
    marginTop: 20,
    position: 'absolute'
  },
  projectBarDate: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 15,
    alignSelf: 'flex-start',
    marginLeft: 13,
    marginTop: 40,
    position: 'absolute'
  },
  projectBarContainer: {
    marginBottom: 10,
    paddingBottom: 300,
  },
  imageProfile: {
    width: 50,
    height: 50,
    marginLeft: 290,
    borderRadius: 100,
  },
});
