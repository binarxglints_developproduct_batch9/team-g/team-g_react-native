import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Header, Icon, Image, Button } from 'react-native-elements';
import { TouchableWithoutFeedback, FlatList } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { Menu } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';


import { GET_PROJECT_ID } from '../../../redux/Case Type/Project';
import { deleteProject } from '../../../redux/Action/ProjectAction';
import { LinearConfig } from '../../../LinearConfig';

const MyLeftComponent = () => {
  return <Text style={styles.headerText}>Project</Text>;
};

const DashboardProject = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const project = useSelector(state => state.project.project);
  const [index, setIndex] = useState(0);
  const [isShowing, setIsShowing] = useState(true);
  const [loading, setLoading] = useState(false);

  
  const showMenu = _id => {
    setIndex(_id);
  };

  const hideMenu = () => {
    setIndex(0);
  };

  const handleDetailNavigation = _id => {
    // console.log("ini data ID Project " , _id)
    dispatch({ type: GET_PROJECT_ID, idProject: _id });
    setLoading(true);
    navigation.navigate('Detail Project');
    setIndex(0);
  }



  const handleEditNavigation = _id => {
    dispatch({ type: GET_PROJECT_ID, project: project });
    navigation.navigate('Edit Project', { idProject: _id });
    setIndex(0);
  };

  const handleDelete = _id => {
    dispatch(deleteProject(_id));
    setIndex(0);
  };

  const renderItem = ({ item }) => (
    <View style={styles.projectCardBar}>
      <TouchableWithoutFeedback onPress={() => handleDetailNavigation(item._id)}>
        <View style={styles.projectBodyContainer}>
          <Image source={{ uri: item.bgImage }} style={styles.projectImage} />
          <Text style={styles.projectCardTitle}>{item.name}</Text>
        </View>
        {/* <Spinner visible={loading}></Spinner> */}
      </TouchableWithoutFeedback>
      <Menu
        visible={index === item._id ? true : false}
        onDismiss={hideMenu}
        anchor={
          <Icon
            type="feather"
            name="more-vertical"
            size={24}
            containerStyle={styles.projectCardMore}
            onPress={() => showMenu(item._id)}
          />
        }>
        <Menu.Item
          title="View"
          onPress={() => handleDetailNavigation(item._id)}
        />
        <Menu.Item title="Edit" onPress={() => handleEditNavigation(item._id)} />
        <Menu.Item title="Delete" onPress={() => handleDelete(item._id)} />
      </Menu>
    </View>
  );

  return (
    <>
      <Header
        leftComponent={<MyLeftComponent />}
        backgroundColor="#FFF"
        containerStyle={styles.containerHeader}
      />
      <View style={styles.container}>
        <Text style={styles.projectTitle}>My Project</Text>
        <View style={styles.container}>
          <FlatList
            data={project}
            renderItem={renderItem}
            keyExtractor={item => item._id}
          />
        </View>
        <View>
          <Button
            buttonStyle={styles.buttonContainer}
            title="Create New Project"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={() => navigation.navigate('Create Project')}
          />
        </View>
      </View>
    </>
  );
};

export default DashboardProject;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
    backgroundColor: '#FFF'
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginLeft: 8,
    marginTop: 70,
    marginBottom: 41,
    color: '#000',
    width: 100,
  },
  projectTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
    marginLeft: 16,
    marginTop: 30,
    marginBottom: 20,
    backgroundColor: '#FFF'
  },
  projectContainer: {
    marginHorizontal: 16,
    marginVertical: 24,
  },
  projectCardContainer: {
    marginHorizontal: 16,
    marginVertical: 10,
    paddingBottom: 75,
  },
  projectCardBar: {
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 3,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 4,
    marginHorizontal: 16
  },
  projectImage: {
    width: 62,
    height: 62,
  },
  projectCardTitle: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginHorizontal: 30,
  },
  projectCardMore: {
    marginHorizontal: 0,
  },
  buttonContainer: {
    height: 60,
    width: 303,
    marginTop: 10,
    marginBottom: 30,
    marginHorizontal: 16,
    alignSelf: 'center',
    borderRadius: 50
  },
  projectButtonContainer: {
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'flex-end',
    bottom: 30,
    right: 30,
  },
  projectBodyContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    height: 23,
    width: 303,
    marginHorizontal: 16,
    paddingHorizontal: 5,
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fff'
  },
  button: {
    marginVertical: 13,
    marginHorizontal: 16,
    backgroundColor: '#FFF',
    alignSelf: 'center'
  },
});
