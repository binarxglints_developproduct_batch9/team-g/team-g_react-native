import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, StatusBar} from 'react-native';
import {
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {Icon, Button, Header} from 'react-native-elements';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import {Menu} from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import {Board} from 'react-native-draganddrop-board';

import TaskCard from '../../task screen/TaskCard';
import {getProjectDetail} from '../../../redux/Action/ProjectAction';
import {GET_TASK_ID} from '../../../redux/Case Type/Task';
import {LinearConfig} from '../../../LinearConfig';

const MyCenterComponent = props => {
  return <Text style={styles.headerText}>{props.title}</Text>;
};

const MyRightComponent = props => {
  const [visible, setVisible] = useState(false);
  const handleNavigate = () => {
    props.navigation.navigate('Member');
    setVisible(false);
  };
  return (
    <Menu
      visible={visible}
      anchor={
        <Icon
          type="feather"
          name="menu"
          size={25}
          color="#000"
          onPress={() => setVisible(!visible)}
        />
      }
      onDismiss={() => setVisible(false)}>
      <Menu.Item title="Member" onPress={handleNavigate} />
    </Menu>
  );
};

const MyLeftComponent = props => {
  return (
    <Icon
      type="feather"
      name="arrow-left"
      size={24}
      containerStyle={styles.icon}
      onPress={props.back}
    />
  );
};

const ProjectDetail = () => {
  const idproject = useSelector(state => state.project.idProject);
  const card = useSelector(state => state.project.cardProject);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const loading = useSelector(state => state.project.isLoading);
  const title = useSelector(state => state.project.title);
  const [swipe, setSwipe] = useState(true);
  const [drag, setDrag] = useState(false);

  useEffect(() => {
    dispatch(getProjectDetail(idproject));
  }, [dispatch, idproject]);

  const handleDetailTaskNavigator = id => {
    dispatch({type: GET_TASK_ID, idTask: id});
    navigation.navigate('Task Detail');
  };

  let matches;
  let initial;

  if (title !== null) {
    matches = title.match(/\b(\w)/g);
    initial = matches.join('');
  }
  const handleSwipe = () => {
    setSwipe(false);
    setDrag(true);
    console.log('long');
  };

  const handleNotSwipe = () => {
    setSwipe(true);
    setDrag(false);
    console.log('out');
  };

  const renderItem = ({item}) => (
    <View style={styles.cardProject}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <View style={styles.cardHeaderContainer}>
        <Text style={styles.header}>{item.name}</Text>
      </View>
      <ScrollView>
        <View style={styles.cardBodyContainer}>
       
          <FlatList
            data={item.task}
            renderItem={({item}) => (
              <TouchableWithoutFeedback
                onPress={() => handleDetailTaskNavigator(item.id)}
                onLongPress={handleSwipe}>
                <TaskCard
                  title={item.title}
                  description={item.description}
                  id={item.id}
                  navigation={navigation}
                  projectId={idproject}
                  initial={initial}
                  assign={item.Users}
                  drag={drag}
                />
              </TouchableWithoutFeedback>
            )}
            keyExtractor={item => item.id}
            extraData={item.Tasks}
          />
        </View>
      </ScrollView>
    </View>
  );

  const handleBack = () => {
    navigation.navigate('Dashboard', {screen: 'Project'});
  };

  let dragView;
  if (drag === true) {
    dragView = (
      <View>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontWeight: '500',
            textAlign: 'center',
            marginVertical: 8,
          }}>
          Swipe Right or Left to Move Task
        </Text>
        <Button
          title="Cancel"
          onPress={handleNotSwipe}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={{
            fontFamily: 'Montserrat',
            fontSize: 14,
            fontWeight: 'bold',
          }}
          buttonStyle={[styles.buttonContainer, {width: 100}]}
        />
      </View>
    );
  }

return (
  <>
    <Header
      leftComponent={<MyLeftComponent back={handleBack} />}
      rightComponent={<MyRightComponent navigation={navigation} />}
      centerComponent={<MyCenterComponent title={title} />}
      backgroundColor="#FFF"
      containerStyle={styles.containerHeader}
    />
    <View style={styles.container}>
      {dragView}
      <View style={styles.cardProjectContainer}>
        <FlatList
          data={card}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          horizontal={true}
          scrollEnabled={swipe}
        />
      </View>
      <View style={styles.projectButtonContainer}>
        <Button
          icon={<Icon type="feather" name="plus" size={35} color="#FFF" />}
          containerStyle={styles.buttonContainer}
          onPress={() => navigation.navigate('Create Task')}
        />
      </View>
      <Spinner visible={loading} size="large" />
    </View>
  </>
);
};
export default ProjectDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CAF0F8' ,
  },
  cardProject: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    backgroundColor: '#72efdd',
    marginRight: 32,
    marginVertical: 14,
    width: 250,
  },
  cardProjectContainer: {
    marginLeft: 16,
    flex: 1,
    
  },
  header: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  cardHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 14,
    
  },
  cardBodyContainer: {
    justifyContent: 'center',
    
  },
  buttonContainer: {
    borderRadius: 100,
    alignSelf: 'center',
  },
  projectButtonContainer: {
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'flex-end',
    bottom: 30,
    right: 30,
  },
  containerHeader: {
    paddingBottom: 10,
    height: 60,
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
  },
  icon: {
    marginHorizontal: 5,
  },
});
