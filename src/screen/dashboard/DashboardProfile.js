import React from 'react';
import {View, Text, StyleSheet, StatusBar, TouchableOpacity} from 'react-native';
import {Header, Image, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {useSelector, useDispatch} from 'react-redux';
import {ScrollView} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import {GoogleSignin} from '@react-native-community/google-signin';

import {ISLOGOUT} from '../../redux/Reducer/case';

const MyLeftComponent = () => {
  return <Text style={styles.headerText}>Profile</Text>;
};

const DashboardProfile = () => {
  const profile = useSelector(state => state.showUser);
  const username = profile.name;
  const biodata = profile.bio;
  const email = profile.email;
  const image = profile.imageProfile;
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleLogout = async () => {
    try {
      AsyncStorage.clear();
      GoogleSignin.signOut();
      dispatch({type: ISLOGOUT});
      navigation.navigate('Landing Page');
      navigation.reset({
        index: 0,
        routes: [{name: 'Landing Page'}],
      });
    } catch (error) {
      console.log(error, 'error');
    }
  };

return (
   <>
    <Header
      leftComponent={<MyLeftComponent />}
      backgroundColor="#FFF"
      containerStyle={styles.containerHeader}
    />
    <View style={styles.container}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
        <ScrollView>
          <View style={styles.imageContainer}>
            <Image source={{uri: image}} style={styles.imageProfile} />
          </View>
          <View style={styles.usernameContainer}>
            <Text style={styles.username}>{username}</Text>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('Payment')}>
            <LinearGradient
              colors={['#FFD200','#f79d00',]}
              style={styles.buttonUpgrade}>
              <Text style={styles.buttonTitleUpgrade}>Premium</Text>
            </LinearGradient>
          </TouchableOpacity>
          <View style={styles.profileContainer}>
          <View style={styles.emailContainer}>
            <Text style={styles.profileTitle}>Email</Text>
            <Text style={styles.email}>{email}</Text>
          </View>
          <View style={styles.biodataContainer}>
            <Text style={styles.profileTitle}>About</Text>
            <Text style={styles.biodata}>{biodata}</Text>
          </View>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('Edit Profile')}>
            <LinearGradient
              colors={['#2DC0CA','#1589CB']}
              style={styles.button}>
              <Text style={styles.buttonTitle}>Edit Profile</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleLogout}>
            <LinearGradient
              colors={['#FFF','#FAFAFA']}
              style={styles.buttonCancel}>
              <Text style={styles.cancelTitle}>Log Out</Text>
            </LinearGradient>
          </TouchableOpacity>
      </ScrollView>
    </View>
  </>
)};

export default DashboardProfile;

const styles = StyleSheet.create({
container: {
   flex: 1,
   backgroundColor: '#FFF',
},
containerHeader: {
   paddingTop: 10,
   height: 60,
   justifyContent: 'center',
   backgroundColor:'#FFF'
},
headerText: {
   fontFamily: 'Montserrat',
   fontWeight: '500',
   fontSize: 18,
   marginLeft:10,
   marginTop: 30,
   color: '#000',
   width: 100,
},
imageProfile: {
   width: 105,
   height: 105,
   borderWidth: 2,
   borderColor: '#1589CB',
   borderRadius: 100,
},
imageContainer: {
   alignSelf: 'center',
   marginVertical: 25,
},
username: {
   fontFamily: 'Montserrat',
   fontWeight: 'bold',
   alignSelf:'center',
   fontSize: 24,
},
usernameContainer: {
   marginHorizontal: 16,
   marginBottom: 24,
},
profileTitle: {
   fontFamily: 'Montserrat',
   fontWeight: 'bold',
   fontSize: 18,
},
email: {
   fontFamily: 'Montserrat',
   fontSize: 18,
   marginVertical: 8,
},
emailContainer: {
   marginHorizontal: 16,
   marginVertical: 8,
},
biodataContainer: {
   marginHorizontal: 16,
   marginVertical: 8,
},
biodata: {
   height:120,
   width: 358,
   fontFamily: 'Montserrat',
   fontSize: 18,
   marginVertical: 8,
},
button: {
   height:60,
   width:358,
   marginVertical: 5,
   marginHorizontal: 16,
   borderRadius: 3,
   alignSelf: 'center',
   backgroundColor:'#FFF',
},
buttonCancel: {
  height:60,
  width:358,
  marginVertical: 5,
  marginHorizontal: 16,
  borderRadius: 3,
  // borderWidth:1,
  alignSelf: 'center',
  backgroundColor:'#FFF',
},
buttonTitle: {
   fontFamily: 'Montserrat',
   height: 23,
   width:358,
   marginTop:15,
   textAlign: 'center',
   justifyContent: 'center',
   alignSelf: 'center',
   fontSize:18,
   fontWeight: '700',
   color:'#fff'
},
buttonUpgrade: {
  height:40,
  width:120,
  marginVertical: 5,
  marginHorizontal: 136,
  alignItems:'center',
  borderRadius: 3,
  backgroundColor:'#FFF',
},
buttonTitleUpgrade: {
  fontFamily: 'Montserrat',
  fontWeight:'bold',
  height: 23,
  width:100,
  marginTop:7,
  textAlign: 'center',
  justifyContent: 'center',
  fontSize:18,
  fontWeight: '700',
  color:'#fefae0'
},
cancelTitle: {
   height: 23,
   width:303,
   fontFamily: 'Montserrat',
   fontWeight: 'bold',
   fontSize: 18,
   marginVertical: 5,
   textAlign:'center',
   alignSelf:'center',
   marginTop: 15,
   textAlign: 'center',
   color: '#1589CB',
  }});
