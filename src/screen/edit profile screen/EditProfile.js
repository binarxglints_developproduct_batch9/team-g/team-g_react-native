import React, { useState } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import { Icon, Image, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { LinearConfig } from '../../LinearConfig';
import { useDispatch, useSelector } from 'react-redux';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import { API_USER } from '../../API';
import { useNavigation } from '@react-navigation/native';
import {
  GET_NAME,
  GET_BIO,
  GET_PROFILE_IMAGE,
} from '../../redux/Case Type/Showuser';

const EditProfile = () => {
  const profile = useSelector(state => state.showUser);
  const username = profile.name;
  const biodata = profile.bio;
  const email = profile.email;
  const image = profile.imageProfile;
  const [fullName, setName] = useState('');
  const [bio, setBio] = useState('');
  const [avatar, setAvatar] = useState('');
  const [filePath, setFilePath] = useState('');
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const onChangeName = val => {
    setName(val);
  };

  const onChangeBio = val => {
    setBio(val);
  };

  const ChooseFile = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        setAvatar(response.uri);
        setFilePath(response);
      }
    });
  };

  const handleUpdateProfile = async () => {
    let dataForm = new FormData();
    if (fullName !== '') {
      dataForm.append('fullName', fullName);
      dispatch({ type: GET_NAME, name: fullName });
    }
    if (bio !== '') {
      dataForm.append('bio', bio);
      dispatch({ type: GET_BIO, bio: bio });
    }
    if (filePath !== '') {
      dataForm.append('image', {
        name: filePath.fileName,
        type: filePath.type,
        uri: filePath.uri,
      });
      dispatch({ type: GET_PROFILE_IMAGE, imageProfile: avatar });
    }
    setLoading(true);
    try {
      const token = await AsyncStorage.getItem('userToken');
      // const res = await Axios.put(`${API_USER}/update`, dataForm, {
      //   headers: {
      //     Authorization: `bearer ${token}`
      //   },
      Axios({
        method: 'PUT',
        url: `${API_USER}/update`,
        data: dataForm,
        headers: {
          Authorization: `bearer ${token}`
        }
      })
        .then(({ data }) => {

          if (data !== null) {
            const status = data.status;
            if (status == 'success') {
              setLoading(false);
              navigation.navigate('Profile');
            } else {
              setLoading(false);
            }
          }
        });
    } catch (error) {
      console.log(error, 'error');
      setLoading(false);
    }
  };
  let avatarImage;
  if (avatar !== '') {
    avatarImage = avatar;
  } else {
    avatarImage = image;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <ScrollView>
        <View style={styles.imageContainer}>
          <Image source={{ uri: avatarImage }} style={styles.imageProfile} />
          <Icon
            type="feather"
            name="edit"
            size={25}
            containerStyle={styles.icon}
            onPress={ChooseFile}
          />
        </View>
        <Button
          title="Change Password"
          containerStyle={{ alignSelf: 'center' }}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          onPress={() => navigation.navigate('Update Password')}
        />
        <View style={styles.usernameContainer}>
          <TextInput
            defaultValue={username}
            style={styles.username}
            onChangeText={val => onChangeName(val)}
          />
        </View>
        <View style={styles.profileContainer}>
          <View style={styles.emailContainer}>
            <View
              style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
              <Text style={styles.profileTitle}>Email</Text>

            </View>
            <Text style={styles.email}>{email}</Text>
          </View>
          <View style={styles.aboutContainer}>
            <Text style={styles.profileTitle}>About</Text>
            <TextInput
              defaultValue={biodata}
              style={styles.about}
              multiline={true}
              onChangeText={val => onChangeBio(val)}
            />
          </View>
        </View>
        <View>
          <Button
            title="Save"
            containerStyle={styles.buttonSave}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={handleUpdateProfile}
          />
          <Button
            title="Cancel"
            containerStyle={styles.buttonCancel}
            titleStyle={styles.cancelTitle}
            buttonStyle={styles.cancelButton}
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
    // '#F4F4F4',
  },
  containerHeader: {
    paddingTop: 10,
    height: 60,
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
    width: 100,
  },
  imageProfile: {
    width: 105,
    height: 105,
    borderWidth: 1,
    marginLeft: 20,
    borderColor: '#1589CB',
    borderRadius: 100,
    alignSelf: 'center'
  },
  imageContainer: {
    alignSelf: 'center',
    marginVertical: 25,
    flexDirection: 'row',
  },
  username: {
    height: 50,
    width: 358,
    marginTop: 20,
    paddingLeft: 5,
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 24,
    backgroundColor: '#FFF',
    borderWidth: 1
  },
  usernameContainer: {
    marginHorizontal: 16,
    marginBottom: 5,
  },
  profileTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  email: {
    height: 50,
    width: 358,
    textAlignVertical: 'center',
    paddingLeft: 5,
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginVertical: 8,
    borderWidth: 1,
    backgroundColor: '#FFF'
  },
  emailContainer: {
    marginHorizontal: 16,
    marginTop: 5,
  },
  aboutContainer: {
    marginHorizontal: 16,
    marginVertical: 5,
  },
  about: {
    height: 120,
    width: 358,
    fontFamily: 'Montserrat',
    fontSize: 18,
    paddingTop: 1,
    paddingLeft: 5,
    marginVertical: 8,
    borderWidth: 1,
    backgroundColor: '#FFF'
  },
  buttonSave: {
    marginTop: 15,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonCancel: {
    marginTop: 10,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  icon: {
    alignSelf: 'flex-end',
    left: -30,
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
});
