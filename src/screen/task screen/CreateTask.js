import React, { useState } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity } from 'react-native';
import {
  TextInput,
  TouchableWithoutFeedback,
  ScrollView,
  FlatList,
} from 'react-native-gesture-handler';
import { Image, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';

import { createTask } from '../../redux/Action';
import addImage from '../../asset/addImage.png';
import { LinearConfig } from '../../LinearConfig';

const CreateTask = () => {
  const [title, setTitle] = useState('');
  const [descrip, setDescrip] = useState('');
  const [attachment, setImage] = useState([]);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const loading = useSelector(state => state.project.isLoading);
  const idProject = useSelector(state => state.project.idProject);
console.log("ini id project "+(idProject))
  const onChangeTitle = val => {
    setTitle(val);
  };

  const onChangeDescrip = val => {
    setDescrip(val);
  };

  const handlePickImage = async () => {
    try {
      const attachment = await ImagePicker.openPicker({
        multiple: true,
        width: 100,
        height: 100,
      });
      console.log(attachment);
      setImage(attachment);
    } catch (error) {
      console.log(error, 'error');
    }
  };

  const handleCreateTask = () => {
    let dataForm = new FormData();
    dataForm.append('name', title);
    dataForm.append('description', descrip);
    if (attachment !== []) {
      attachment.forEach((pict, index) => {
        dataForm.append('attachment', {
          uri: pict.path,
          type: pict.mime,
          name: `picture[${index}]`,
        });
      });
    }
    dispatch(createTask(idProject, dataForm, navigation));
  };

  const renderItem = ({ item }) => (
    <TouchableWithoutFeedback onPress={handlePickImage}>
      <Image source={{ uri: item.path }} containerStyle={styles.image} />
    </TouchableWithoutFeedback>
  );

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

      <View >
        <Text style={styles.title}>Title</Text>
        <TextInput
          placeholder="Title"
          style={styles.titleInput}
          onChangeText={onChangeTitle}
        />
      </View>
      <View >
        <Text style={styles.title}>Description</Text>
        <TextInput
          placeholder="Description"
          multiline={true}
          style={styles.descriptionInput}
          onChangeText={onChangeDescrip}
        />
      </View>
      <View >
        <Text style={styles.title}>Task Image</Text>
        <View>
          <FlatList
            data={attachment}
            renderItem={renderItem}
            keyExtractor={item => item.path}
            horizontal={true}
          />
          <ScrollView horizontal>
            <TouchableWithoutFeedback onPress={handlePickImage}>
              <Image source={addImage} containerStyle={styles.image} />
            </TouchableWithoutFeedback>
          </ScrollView>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={handleCreateTask}>
          <LinearGradient
            colors={['#2DC0CA', '#1589CB']}
            style={styles.button}>
            <Text style={styles.buttonTitle}>Create Task</Text>
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <LinearGradient
            colors={['#FFF', '#FAFAFA']}
            style={styles.cancelButton}>
            <Text style={styles.cancelTitle}>Cancel</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      <Spinner visible={loading} size="large" />
    </View>
  );
};

export default CreateTask;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  titleContainer: {
    marginHorizontal: 16,
    marginTop: 28,
  },
  title: {
    height: 30,
    width: 358,
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 10,
    marginLeft: 16,
  },
  titleInput: {
    height: 60,
    width: 358,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginLeft: 16,
    marginTop: 3,
    height: 100,
    textAlignVertical: 'top',
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionInput: {
    height: 60,
    width: 358,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginLeft: 16,
    marginTop: 3,
    height: 100,
    textAlignVertical: 'top',
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 10,
    marginBottom: 40,
    marginLeft: 16
  },
  imageContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  button: {
    height: 60,
    width: 303,
    marginBottom: 10,
    marginHorizontal: 16,
    borderRadius: 50,
    alignSelf:'center',
    backgroundColor: '#FFF',
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    height: 23,
    width: 303,
    marginTop: 15,
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fff'
  },
  cancelTitle: {
    height: 23,
    width: 303,
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    marginTop: 15,
    textAlign: 'center',
    color: '#1589CB',
  },
  cancelButton: {
    height: 60,
    width: 303,
    marginBottom: 10,
    marginHorizontal: 16,
    alignSelf:'center',
    borderRadius: 50,
    backgroundColor: '#FFF',
    borderWidth: 1
  },
  buttonContainer: {
    marginTop: 50,
  },
  assignContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  inputTag: {
    backgroundColor: '#E5E5E5',
  },
  inputContainer: {
    marginTop: 8,
    alignSelf: 'flex-start',
  },
});
