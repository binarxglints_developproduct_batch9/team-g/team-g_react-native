import React, {useEffect, useState} from 'react';
import {View, Dimensions} from 'react-native';
import {Text} from 'react-native';
import {StyleSheet} from 'react-native';
import {
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {Image, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useSelector, useDispatch} from 'react-redux';
import {getTask, deleteTask} from '../../redux/Action';
import {useNavigation} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import {Modal} from 'react-native-paper';

const TaskDetail = () => {
  const idTask = useSelector(state => state.task.idTask);
  const [visible, setVisible] = useState(false);
  const [fullimage, setFullimage] = useState('');
  const dispatch = useDispatch();
  const task = useSelector(state => state.task);
  const taskTitle = task.title;
  const taskDescrip = task.description;
  // const assignment = task.assignment;
  const assignment = useSelector(state => state.task.assignment);
  const navigation = useNavigation();
  const loading = useSelector(state => state.task.isLoading);
  const width = Math.round(Dimensions.get('window').width);
  const height = Math.round(Dimensions.get('window').height);
  const [ratio, setRatio] = useState(null);

  useEffect(() => {
    dispatch(getTask(idTask));
  }, [dispatch, idTask]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const orientation = () => {
    if (width < height) {
      setRatio('potrait');
    } else {
      setRatio('landscape');
    }
  };

  useEffect(() => {
    orientation();
    Dimensions.addEventListener('change', () => {
      orientation();
    });
  }, [orientation]);

  const handleDeleteTask = () => {
    dispatch(deleteTask(idTask));
    navigation.navigate('Project Detail');
    navigation.reset({
      index: 0,
      routes: [{name: 'Project Detail'}],
    });
  };

  const handlePressImage = uri => {
    setVisible(true);
    setFullimage(uri);
  };

  const renderItem = ({item}) => (
    <TouchableWithoutFeedback onPress={() => handlePressImage(item.image_url)}>
      <Image source={{uri: item.image_url}} style={styles.image} />
    </TouchableWithoutFeedback>
  );

  const renderCard = ({item}) => (
    <View style={styles.assignCard}>
      <Text style={styles.cardEmail}>{item.email}</Text>
    </View>
  );

  let assign;
  if (assignment.length > 0) {
    assign = (
      <View style={styles.titleContainer}>
        <Text style={styles.header}>Assgined To</Text>
        <View style={styles.cardContainer}>
          <FlatList
            data={assignment}
            renderItem={renderCard}
            horizontal={true}
            keyExtractor={item => item.id_assignment}
          />
        </View>
      </View>
    );
  }

  let attachment;
  if (task.image.length > 0) {
    attachment = (
      <View style={styles.titleContainer}>
        <Text style={styles.header}>Attachment</Text>
        <FlatList
          data={task.image}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          horizontal={true}
        />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.titleContainer}>
          <Text style={styles.header}>Title</Text>
          <Text style={styles.bodyTitle}>{taskTitle}</Text>
        </View>
        <View style={styles.titleContainer}>
          <Text style={styles.header}>Description</Text>
          <Text style={styles.bodyDesc}>{taskDescrip}</Text>
        </View>
        {attachment}
        {assign}
        <View>
          <Button
            title="Edit Task"
            containerStyle={styles.button}
            ViewComponent={LinearGradient}
            linearGradientProps={LinearConfig}
            titleStyle={styles.buttonTitle}
            onPress={() => navigation.navigate('Edit Task')}
          />
          <Button
            title="Delete"
            containerStyle={styles.button}
            titleStyle={styles.cancelTitle}
            buttonStyle={styles.cancelButton}
            onPress={handleDeleteTask}
          />
        </View>
      </ScrollView>
      <Spinner visible={loading} size="large" />
      <Modal visible={visible} onDismiss={() => setVisible(false)}>
        <ScrollView>
          <View
            style={{
              flex: 1,
              padding: 16,
            }}>
            <Image
              source={{uri: fullimage}}
              style={{
                aspectRatio: ratio === 'potrait' ? 2 / 3 : 9 / 16,
                maxWidth: width,
                maxHeight: ratio === 'landscape' ? null : height,
                height: ratio === 'potrait' ? '100%' : undefined,
                width: ratio === 'landscape' ? '100%' : undefined,
              }}
            />
          </View>
        </ScrollView>
      </Modal>
    </View>
  );
};

export default TaskDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  header: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  bodyTitle: {
    height:50,
    width:358,
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginTop: 8,
  },
  bodyDesc: {
    height:200,
    width:358,
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginTop: 8,
  },
  titleContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  image: {
    width: 200,
    height: 200,
    marginTop: 8,
    marginRight: 8,
  },
  assignCard: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 7,
    marginVertical: 8,
    marginHorizontal: 8,
    flexDirection: 'row',
  },
  cardContainer: {
    alignContent: 'flex-start',
    flexDirection: 'row',
    alignSelf: 'flex-start',
    backgroundColor: '#E5E5E5',
    marginTop: 8,
  },
  cardEmail: {
    fontFamily: 'Montserrat',
    fontSize: 12,
    color: '#898989',
    marginVertical: 8,
    marginHorizontal: 18,
  },
  button: {
    marginTop: 15,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
  fullimage: {
    width: undefined,
    height: undefined,
  },
  icon: {
    alignSelf: 'center',
    marginRight: 8,
  },
});
