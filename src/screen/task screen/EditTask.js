import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import {
  TextInput,
  TouchableWithoutFeedback,
  ScrollView,
  FlatList,
} from 'react-native-gesture-handler';
import {Image, Button, Icon} from 'react-native-elements';
import addImage from '../../asset/addImage.png';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../../LinearConfig';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';
import {
  editTask,
  assignmentTask,
  assignmentDelete,
  deleteImage,
} from '../../redux/Action';
import DropDownPicker from 'react-native-dropdown-picker';
import AwesomeAlert from 'react-native-awesome-alerts';
import {TASK_ALERT} from '../../redux/Case Type/Task';
import {Modal} from 'react-native-paper';

const EditTask = () => {
  const [title, setTitle] = useState('');
  const [descrip, setDescrip] = useState('');
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const loading = useSelector(state => state.task.isLoading);
  const task = useSelector(state => state.task);
  const assignment = task.assignment;
  const [image, setImage] = useState([]);
  const [value, setValue] = useState('');
  const idProject = useSelector(state => state.project.idProject);
  const [email, setEmail] = useState('');
  const alert = useSelector(state => state.task.alert);
  const message = useSelector(state => state.task.message);
  const taskImage = useSelector(state => state.task.image);
  const [visible, setVisible] = useState(false);
  const [fullimage, setFullimage] = useState(null);
  const width = Math.round(Dimensions.get('window').width);
  const height = Math.round(Dimensions.get('window').height);
  const [ratio, setRatio] = useState(null);
  const [idImage, setIdImage] = useState(null);
  const [imageAlert, setImageAlert] = useState(false);
  const [arrImage, setArrImage] = useState([]);

  const onChangeTitle = val => {
    setTitle(val);
  };

  const onChangeEmail = val => {
    setEmail(val);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const orientation = () => {
    if (width < height) {
      setRatio('potrait');
    } else {
      setRatio('landscape');
    }
  };

  useEffect(() => {
    orientation();
    Dimensions.addEventListener('change', () => {
      orientation();
    });
  }, [orientation]);

  const items = [
    {
      label: 'Backlog',
      value: 'Backlog',
    },
    {
      label: 'Todo',
      value: 'Todo',
    },
    {
      label: 'Progress',
      value: 'Progress',
    },
    {
      label: 'Done',
      value: 'Done',
    },
  ];

  const onSelected = val => {
    setValue(val);
  };

  const onChangeDescrip = val => {
    setDescrip(val);
  };

  const handlePickImage = async () => {
    try {
      const images = await ImagePicker.openPicker({
        multiple: true,
        width: 100,
        height: 100,
      });
      console.log(images);
      setImage(images);
    } catch (error) {
      console.log(error, 'error');
    }
  };

  const handlePutImage = item => {
    return item.map((obj, index) => {
      return Object.assign({}, obj, {id: index});
    });
  };

  let arr = [];

  if (taskImage.length > 0) {
    taskImage.forEach(item => {
      arr.push({id: item.id, image: item.image_url});
    });
  }

  if (image.length > 0) {
    const arrTest = handlePutImage(image);
    arrTest.forEach(item => {
      arr.push({id: item.id, image: item.path});
    });
  }

  if (arrImage.length > 0) {
    arr = arrImage;
  }

  const handleEditTask = () => {
    let dataForm = new FormData();
    if (title !== '') {
      dataForm.append('title', title);
    }
    if (descrip !== '') {
      dataForm.append('description', descrip);
    }
    if (image !== []) {
      image.forEach((pict, index) => {
        dataForm.append('image', {
          uri: pict.path,
          type: pict.mime,
          name: `picture[${index}]`,
        });
      });
    }
    if (value !== '') {
      dataForm.append('card_id', `${value}${idProject}`);
    }
    dispatch(editTask(task.idTask, dataForm, navigation));
  };

  const handleImageDetail = (uri, id) => {
    setVisible(true);
    setFullimage(uri);
    setIdImage(id);
  };

  const handleDeleteImage = id => {
    const ari = arr.filter(item => {
      return item.id !== id;
    });
    dispatch(deleteImage(id));
    setImageAlert(false);
    setVisible(false);
    setArrImage(ari);
  };

  const renderItem = ({item}) => (
    <TouchableWithoutFeedback
      onPress={() => handleImageDetail(item.image, item.id)}>
      <Image source={{uri: item.image}} containerStyle={styles.image} />
    </TouchableWithoutFeedback>
  );

  const renderCard = ({item}) => (
    <View style={styles.assignCard}>
      <Text style={styles.cardEmail}>{item.email}</Text>
      <Icon
        type="feather"
        name="x-circle"
        size={15}
        containerStyle={styles.iconCard}
        onPress={() => dispatch(assignmentDelete(item.id, assignment))}
      />
    </View>
  );

  return (
    <>
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>Title</Text>
            <TextInput
              defaultValue={task.title}
              style={styles.titleInput}
              onChangeText={onChangeTitle}
            />
          </View>
          <View style={styles.descriptionContainer}>
            <Text style={styles.title}>Description</Text>
            <TextInput
              defaultValue={task.description}
              multiline={true}
              style={styles.descriptionInput}
              onChangeText={onChangeDescrip}
            />
          </View>
          <View style={styles.imageContainer}>
            <Text style={styles.title}>Project Image</Text>
            <View style={styles.imageList}>
              <FlatList
                data={arr}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                horizontal={true}
              />
              <TouchableWithoutFeedback onPress={handlePickImage}>
                <Image source={addImage} containerStyle={styles.image} />
              </TouchableWithoutFeedback>
            </View>
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.header}>Assgined To</Text>
            <View style={styles.cardContainer}>
              <FlatList
                data={assignment}
                renderItem={renderCard}
                horizontal={true}
                keyExtractor={item => item.id}
              />
            </View>
            <View style={styles.assignContainer}>
              <TextInput
                placeholder="Input Email"
                style={styles.inputCard}
                multiline={true}
                onChangeText={onChangeEmail}
              />
              <Button
                title="Assign"
                containerStyle={{alignSelf: 'center'}}
                ViewComponent={LinearGradient}
                linearGradientProps={LinearConfig}
                onPress={() =>
                  dispatch(assignmentTask(task.idTask, email, assignment))
                }
              />
            </View>
          </View>
          <View style={styles.buttonContainer}>
            <DropDownPicker
              items={items}
              onChangeItem={item => onSelected(item.value)}
              itemStyle={{
                justifyContent: 'flex-start',
              }}
              style={styles.dropdown}
              placeholder="Move to"
            />
            <Button
              title="Save Change"
              containerStyle={styles.button}
              ViewComponent={LinearGradient}
              linearGradientProps={LinearConfig}
              titleStyle={styles.buttonTitle}
              onPress={handleEditTask}
            />
            <Button
              title="Cancel"
              containerStyle={styles.button}
              titleStyle={styles.cancelTitle}
              buttonStyle={styles.cancelButton}
              onPress={() => navigation.goBack()}
            />
          </View>
        </ScrollView>
        <Spinner visible={loading} size="large" />
        <Modal visible={visible} onDismiss={() => setVisible(false)}>
          <Icon
            type="feather"
            name="x-circle"
            size={25}
            containerStyle={{
              alignSelf: 'flex-end',
              top: 10,
              position: 'absolute',
              zIndex: 1,
              right: 10,
            }}
            color="#FFF"
            onPress={() => setImageAlert(true)}
          />
          <ScrollView>
            <View style={{padding: 16, flex: 1}}>
              <Image
                source={{uri: fullimage}}
                style={{
                  aspectRatio: ratio === 'potrait' ? 2 / 3 : 9 / 16,
                  maxWidth: width,
                  maxHeight: ratio === 'landscape' ? null : height,
                  height: ratio === 'potrait' ? '100%' : undefined,
                  width: ratio === 'landscape' ? '100%' : undefined,
                }}
              />
            </View>
          </ScrollView>
        </Modal>
      </View>
      <AwesomeAlert
        show={alert}
        showProgress={false}
        title="Oops"
        message={message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        confirmText="Oke"
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          dispatch({type: TASK_ALERT});
        }}
      />
      <AwesomeAlert
        show={imageAlert}
        showProgress={false}
        title="Confirmation"
        message="Are you sure want to delete the image?"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={true}
        showConfirmButton={true}
        cancelText="No, cancel"
        confirmText="Yes, delete it"
        showCancelButton={true}
        onCancelPressed={() => {
          setImageAlert(false);
        }}
        confirmButtonColor="#DD6B55"
        onConfirmPressed={() => {
          handleDeleteImage(idImage);
        }}
      />
    </>
  );
};

export default EditTask;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  titleContainer: {
    marginHorizontal: 16,
    marginTop: 28,
  },
  cardEmail: {
    fontFamily: 'Montserrat',
    fontSize: 12,
    color: '#898989',
    marginVertical: 8,
    marginHorizontal: 18,
  },
  assignCard: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 7,
    marginVertical: 8,
    marginHorizontal: 8,
    flexDirection: 'row',
  },
  cardContainer: {
    alignContent: 'flex-start',
    flexDirection: 'row',
    alignSelf: 'flex-start',
    backgroundColor: '#E5E5E5',
    marginTop: 8,
  },
  header: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  iconCard: {
    alignSelf: 'center',
    marginRight: 8,
  },
  title: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
  },
  titleInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  dropdown: {
    marginHorizontal: 16,
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
    color: '#000',
  },
  icon: {
    marginHorizontal: 5,
  },
  containerHeader: {
    paddingBottom: 10,
    height: 60,
    justifyContent: 'center',
  },
  descriptionInput: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    marginTop: 8,
    height: 100,
    textAlignVertical: 'top',
    fontFamily: 'Montserrat',
    fontSize: 18,
  },
  descriptionContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 8,
    marginHorizontal: 4,
  },
  imageContainer: {
    marginHorizontal: 16,
    marginTop: 24,
  },
  button: {
    marginVertical: 10,
    marginHorizontal: 16,
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  cancelTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
    color: '#07689F',
  },
  fullimage: {
    width: '100%',
    height: undefined,
    aspectRatio: 1 / 1,
    paddingHorizontal: 16,
  },
  cancelButton: {
    backgroundColor: '#FAFAFA',
  },
  buttonContainer: {
    marginTop: 16,
  },
  assignContainer: {
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputTag: {
    backgroundColor: '#E5E5E5',
  },
  inputContainer: {
    marginTop: 8,
    alignSelf: 'flex-start',
  },
  inputCard: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 3,
    paddingHorizontal: 14,
    width: 250,
  },
});
