import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Image} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {
  editSwipeRightProgressTask,
  editSwipeLeftProgressTask,
} from '../../redux/Action';
import {FlatList} from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';

const TaskCard = props => {
  const dispatch = useDispatch();
  const moving = useSelector(state => state.task.moving);

  const handleSwipeRight = id => {
    dispatch(editSwipeRightProgressTask(id, props.navigation, props.projectId));
  };

  const handleSwipeLeft = id => {
    dispatch(editSwipeLeftProgressTask(id, props.navigation, props.projectId));
  };

  const renderItem = ({item}) => (
    <Image
      source={{uri: item.profile.picture.image_url}}
      style={styles.footerImage}
    />
  );

  const renderLeftActions = () => {
    return (
      <View>
        <Text
          style={{
            fontFamily: 'Montserrat',
            maxWidth: 60,
            marginLeft: 8,
          }}>
          {moving !== null ? moving : 'Move to right'}
        </Text>
      </View>
    );
  };

  const renderRightActions = () => {
    return (
      <View>
        <Text
          style={{
            fontFamily: 'Montserrat',
            maxWidth: 60,
            marginRight: 8,
          }}>
          {moving !== null ? moving : 'Move to left'}
        </Text>
      </View>
    );
  };

  return (
    <Swipeable
      friction={1}
      overshootFriction={8}
      enabled={props.drag}
      leftThreshold={300}
      rightThreshold={300}
      renderLeftActions={renderLeftActions}
      onSwipeableLeftOpen={() => handleSwipeRight(props.id)}
      renderRightActions={renderRightActions}
      onSwipeableRightOpen={() => handleSwipeLeft(props.id)}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>{props.title}</Text>
        </View>
        <View style={styles.body}>
          <Text style={styles.bodyText}>{props.description}</Text>
        </View>
        <View style={styles.footer}>
          <Text style={styles.footerText}>{props.initial}</Text>
          <View style={styles.footerImageContainer}>
            <FlatList
              data={props.assign}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              scrollEnabled={false}
              horizontal={true}
            />
          </View>
        </View>
      </View>
    </Swipeable>
  );
};

export default TaskCard;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#FFF',
    margin: 8,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },
  headerText: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    fontWeight: '500',
  },
  header: {
    marginTop: 16,
    marginLeft: 16,
  },
  bodyText: {
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  body: {
    marginLeft: 16,
    marginTop: 8,
  },
  footerText: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    color: '#898989',
  },
  footer: {
    marginHorizontal: 16,
    marginTop: 14,
    flexDirection: 'column',
  },
  footerImage: {
    borderRadius: 100,
    width: 40,
    height: 40,
    alignSelf: 'flex-end',
  },
  footerImageContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    marginVertical: 8,
  },
});
