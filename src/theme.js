export const darkTheme = {
    mode: "dark",
    PRIMARY_BACKGROUND_COLOR:"#212121",
    PRIMARY_TEXT_COLOR:"#FFF",
    PRIMARY_BUTTON_COLOR:"#23A8D9",
    PRIMARY_BUTTON_TEXT_COLOR:"#FFF",
    STATUS_BAR_STYLE:"LIGHT-CONTENT"
}

export const lightTheme = {
    mode: "ligth",
    PRIMARY_BACKGROUND_COLOR:"#FFF",
    PRIMARY_TEXT_COLOR:"#212121",
    PRIMARY_BUTTON_COLOR:"#23A8D9",
    PRIMARY_BUTTON_TEXT_COLOR:"#AFFFFF",
    STATUS_BAR_STYLE:"DARK-CONTENT"
}