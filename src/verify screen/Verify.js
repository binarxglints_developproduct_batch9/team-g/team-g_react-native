import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {LinearConfig} from '../LinearConfig';
import {useNavigation} from '@react-navigation/native';

const Verify = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Text
        style={{
          fontFamily: 'Montserrat',
          fontWeight: 'bold',
          fontSize: 20,
          textAlign: 'center',
          marginVertical: 18,
        }}>
        We already Sent verification email
      </Text>
      <Text
        style={{
          fontFamily: 'Montserrat',
          fontWeight: '500',
          fontSize: 18,
          textAlign: 'center',
        }}>
        Plese Check your Email...
      </Text>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginVertical: 20,
        }}>
        <Text
          style={{
            fontFamily: 'Montserrat',
            fontSize: 18,
            fontWeight: '500',
            marginVertical: 8,
          }}>
          Finished Verify?
        </Text>
        <Button
          title="Go to Login Page"
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          containerStyle={{
            marginVertical: 8,
          }}
          onPress={() => navigation.navigate('Login')}
        />
      </View>
    </View>
  );
};

export default Verify;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
