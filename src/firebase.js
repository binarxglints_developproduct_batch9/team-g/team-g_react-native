import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import '@react-native-firebase/messaging';

const firebaseConfig = {
  apiKey: ' AIzaSyBqeizZ9laeI8b0zB5YqNvmc28pO-lxCZg ',
  authDomain: 'protra-836b2.firebaseio.com',
  databaseURL: 'https://protra-836b2.firebaseio.com',
  storageBucket: 'protra-836b2.appspot.com',
  projectId: 'protra-836b2',
  messagingSenderId: '647805002874',
  appId: '1:647805002874:android:86a82992700b1b289407c3',
};

firebase.initializeApp({}, firebaseConfig);
export const auth = firebase.auth();
export const messaging = firebase.messaging();
