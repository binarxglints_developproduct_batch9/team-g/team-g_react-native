import React, { Component } from 'react';
import { Modal, StyleSheet, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { StatusBar } from 'react-native';
import { View, Text, TouchableOpacity, } from 'react-native';
import { WebView } from 'react-native-webview';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';

import { GoldGradient } from '../GoldGradient'
import paypal from '../asset/paypal.png';
import bca from '../asset/bcaico.jpeg';
import mandiri from '../asset/mandiriico.png';
import bni from '../asset/bniico.png';
import { LinearConfig } from '../LinearConfig';
import { MaterialBottomTabView } from '@react-navigation/material-bottom-tabs';

// const navigation = useNavigation();
export default class payment extends React.Component {
  state = {
    showModal: false,
    status: "Pending"
  }

  handleResponse = data => {
    if (data.title === "success") {
      this.setState({ showModal: false, status: "Complete" });
    } else if (data.title === "cancel") {
      this.setState({ showModal: false, status: "Failed" });
    } else {
      return;
    }
  }
  render() {
    const { navigation } = this.props
    return (
      <View style={styles.container}
      >
        <StatusBar backgroundColor='#FFF'></StatusBar>
        <View style={styles.pay}
        >
          <Modal
            visible={this.state.showModal}
            onRequestClose={() => this.setState({ showModal: false })}
          >
            <WebView
              source={{ uri: 'http://10.0.2.2:3000/' }}
              onNavigationStateChange={data => this.handleResponse(data)}
              injectedJavaScript={'document.f1.submit()'}
            // document.getElementById("price").value="123"; http://127.0.0.1:3000/ http://10.0.2.2:3000
            />
          </Modal>
          <View style={styles.ordercontainer}>
            <Text style={styles.OrderDetail}>Order Detail</Text>
            <View style={styles.spaceContainer}>
            </View>
            <Button
              title='Protra'
              containerStyle={styles.buttonSave}
              ViewComponent={LinearGradient}
              linearGradientProps={GoldGradient}
              titleStyle={styles.buttonTitle}>
            </Button>
            <Text style={styles.touchtext}>Premium</Text>
            <View style={styles.spaceContainer}>
            </View>
          </View>
          <Text style={styles.Price}>Price</Text>
          <Text style={styles.PaymentAmount}>$ 3.59</Text>
          <View style={styles.spaceContainer}>
          </View>
          <Text style={styles.PaymentStatus}>Payment Status: {this.state.status}</Text>
          <View style={styles.spaceContainer}>
          </View>
          <Text style={styles.paymentmethod}>Payment Method</Text>
          <View style={styles.touchcontainer}>
            <TouchableOpacity
              onPress={() => this.setState({ showModal: true })}
            >
              <Image source={bca} style={styles.logo} />
              <Text style={styles.touchtext}>BCA Transfer</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('Midtrans')}
            >
              <Image source={mandiri} style={styles.logo} />
              <Text style={styles.touchtext}>Mandiri Transfer</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.setState({ showModal: true })}
            >
              <Image source={bni} style={styles.logo} />
              <Text style={styles.touchtext}>BNI Transfer</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.touchcontainer2}>
            <TouchableOpacity
              onPress={() => this.setState({ showModal: true })}
            >
              <Image source={paypal} style={styles.logo2} />
              <Text style={styles.touchtext2}>Paypal</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CAF0F8',
    justifyContent: 'flex-start',
  },
  pay: {
    flex: 1,
    backgroundColor: '#CAF0F8',
    marginTop: 40
  },
  logo: {
    height: 35,
    width: 80,
    marginTop: 10,
    marginLeft: 23,
    resizeMode: 'cover',
    marginRight: 25
  },
  logo2: {
    height: 40,
    width: 80,
    marginTop: 5,
    resizeMode: 'contain',
  },
  ordercontainer: {
    marginBottom: 20,
  },
  touchcontainer: {
    flexDirection: 'row',
    marginStart: 5,
    marginEnd: 15
  },
  touchcontainer2: {
    marginTop: 20,
    alignSelf: 'center'
  },
  touchtext: {
    alignSelf: 'center',
    fontWeight: 'bold',
    marginVertical: 10
  },
  touchtext2: {
    alignSelf: 'center',
    fontWeight: 'bold'
  },
  paymentmethod: {
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: 30,
    marginBottom: 20
  },
  OrderDetail: {
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  Price: {
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginBottom: 10
  },
  PaymentStatus: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginVertical: 20
  },
  PaymentAmount: {
    fontSize: 20,
    alignSelf: 'center',
    marginBottom: 10
  },
  buttonUpgrade: {
    height: 40,
    width: 120,
    marginVertical: 5,
    marginHorizontal: 136,
    alignItems: 'center',
    borderRadius: 3,
    borderStartWidth: 1,
    borderBottomWidth: 2,
    backgroundColor: '#FFF',
  },
  buttonTitleUpgrade: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    height: 23,
    width: 100,
    marginTop: 7,
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fefae0'
  },
  buttonSave: {
    height: 50,
    width: 100,
    marginTop: 25,
    alignSelf: 'center',
    borderRadius: 3,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  spaceContainer: {
    height: 5,
    width: '100%',
    backgroundColor: '#FFF',
    marginVertical: 5,
    marginHorizontal: 0,
  },
});
