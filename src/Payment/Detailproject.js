// import { MaterialBottomTabView } from '@react-navigation/material-bottom-tabs';
import React, { useEffect, useState } from 'react';
import { Menu } from 'react-native-paper';
import { View, Text, StyleSheet } from 'react-native';
import { Icon, Button, Header } from 'react-native-elements';
import { BoardRepository } from 'react-native-draganddrop-board';
import { Board } from 'react-native-draganddrop-board';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Axios from 'axios';
import { API_TASKS } from '../API';
import { getTask } from '../redux/Action/TaskAction'

const Detailproject = () => {
    const [boardRepository, setBoardRepository] = useState([])
    const [visible, setVisible] = useState(false);
    const dispatch = useDispatch();
    // const [idproject, setIdProject] = useState ()
    const idproject = useSelector(state => state.project.idProject);
    const [isShowing, setIsShowing] = useState(true);
    const navigation = useNavigation();
    
    const handleBack = () => {
        navigation.navigate('Dashboard', { screen: 'Project' });
    };
    // useEffect(() => {
    //     dispatch(getTask(idproject));
    // }, [dispatch, idproject]);


    // useEffect(() => {
    //     const Refresh = setInterval(() => {
    //       setIsShowing(!isShowing);
    //     }, 1);
    //     return () => clearInterval(Refresh);
    //   })


    // const newBoardRepository = () => {
    //     setBoard(board);
    // }
    const MyCenterComponent = props => {
        return <Text style={styles.headerText}>{props.name}</Text>;
    };
    const MyRightComponent = props => {
        const handleNavigate = () => {
            props.navigation.navigate('Member');
            setVisible(false);
        };
        return (
            <Menu
                visible={visible}
                anchor={
                    <Icon
                        type="feather"
                        name="menu"
                        size={25}
                        color="#000"
                        onPress={() => setVisible(!visible)}
                    />
                }
                onDismiss={() => setVisible(false)}>
                <Menu.Item title="Member" onPress={handleNavigate} />
            </Menu>
        );
    };
    const MyLeftComponent = props => {
        return (
            <Icon
                type="feather"
                name="arrow-left"
                size={24}
                containerStyle={styles.icon}
                onPress={props.back}
            />
        );
    };
    const board = []
    Axios({
        method: 'GET',
        url: `${API_TASKS}/getAllTask/${idproject}`,
        headers: {
            Authorization: `bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjAwNmRjN2RkNWU4MTkyMjZkMWVmMzRiIiwiZW1haWwiOiJ0ZWFtZ3BvcnRyYUBnbWFpbC5jb20ifSwiaWF0IjoxNjEyNzgxMTk3fQ.m86_TETV913cuIihhRCzJqTBlzQCPqDfopW7MIwfip4`
        }
    })
        .then(({ data }) => {
            var dnd1 = []
            var loop1 = 1
            for (const prop in data.data) {
                // if (dnd1.length>0){
                //     continue;
                // }
                var arrxrow1 = []
                // console.log("id backlog =>" + JSON.stringify(prop));
                var DataBacklog1 = data.data[prop];
                // console.log("data backlog =>" + JSON.stringify(DataBacklog1));
                for (const pTask in DataBacklog1.tasks) {
                    var DataTask1 = DataBacklog1.tasks[pTask];
                    // console.log("data task =>" + JSON.stringify(DataTask1));
                    // console.log("data task =>" + JSON.stringify(DataTask1.members));
                    var brow1 = new cRows(DataTask1.position, DataTask1.name, DataTask1.description);
                    arrxrow1.push(brow1)
                }

                var sboard1 = new cBoard(loop1, DataBacklog1.name, arrxrow1);
                dnd1.push(sboard1)
                loop1 = loop1 + 1
            }
            // console.log("data board =>" + JSON.stringify(dnd1));
            let repository = new BoardRepository(dnd1)
            setBoardRepository(repository)
            // const [boardRepository] = useState(new BoardRepository(dnd1));
        })
        .catch(error => {
            console.log('error', error);
            // })
        });

    class cBoard {
        constructor(id, name, rows) {
            this.id = id;
            this.name = name;
            this.rows = rows;
        }
    }
    class cRows {
        constructor(id, name, description) {
            this.id = id;
            this.name = name;
            this.description = description;
        }
    }

    return (
        <>
            <Header
                leftComponent={<MyLeftComponent back={handleBack} />}
                rightComponent={<MyRightComponent navigation={navigation} />}
                //   centerComponent={<MyCenterComponent title={title} />}
                backgroundColor="#FFF"
                containerStyle={styles.containerHeader}
            />
            <View style={styles.container}>
                {boardRepository.length !== 0 &&
                    <Board
                        style={styles.Board}
                        boardRepository={boardRepository}
                        open={() => {}}
                        onDragEnd={(srcColumnId, destColumnId, draggedItem) => {}}
                    />
                }
                <View style={styles.projectButtonContainer}>
                    <Button
                        icon={<Icon type="feather" name="plus" size={35} color="#FFF" />}
                        containerStyle={styles.buttonContainer}
                        onPress={() => navigation.navigate('Create Task')}
                    />
                </View>
            </View>
        </>
    )
}

export default Detailproject;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 5
    },
    buttonContainer: {
        borderRadius: 100,
        alignSelf: 'center',
    },
    projectButtonContainer: {
        position: 'absolute',
        zIndex: 1,
        alignSelf: 'flex-end',
        bottom: 30,
        right: 30,
    },
    headerText: {
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    }
})
