import React, { Component } from 'react';
import { Modal, StyleSheet, Image } from 'react-native';
import { WebView } from 'react-native-webview';
import { View, Text, TouchableOpacity, } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';


const Midtrans = () => {
  return async dispatch => {
    dispatch({ type: LOADING, isLoading: true });
    try {
      const token = await AsyncStorage.getItem('userToken');
      Axios({
        method: 'POST',
        url: `https://protra.kuyrek.com/payment/create_midtrans`,
        headers: {
          Authorization: `bearer ${token}`
        }
      })
        .then(({ data }) => {
          console.log(data)
        })
    }
    catch (error) {
      console.log(error, 'error');
    }

    return (
      <View style={styles.container}
      >
        <WebView
          source={{ url }}
        />
      </View>
    )
  }
}
export default Midtrans;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CAF0F8',
    justifyContent: 'flex-start',
  },
})