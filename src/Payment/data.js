import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, StatusBar, TouchableOpacity} from 'react-native';
import { BoardRepository } from 'react-native-draganddrop-board';
import { Board } from 'react-native-draganddrop-board';
import {
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {Icon, Button, Header} from 'react-native-elements';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import {Menu} from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';

// import data from './data'
import TaskCard from '../screen/task screen/TaskCard';
import {getProjectDetail} from '../redux/Action/ProjectAction';
import {GET_TASK_ID} from '../redux/Case Type/Task';
// import {LinearConfig} from '../../../LinearConfig';

const MyCenterComponent = props => {
  return <Text style={styles.headerText}>{props.title}</Text>;
};

const MyRightComponent = props => {
  const [visible, setVisible] = useState(false);
  const handleNavigate = () => {
    props.navigation.navigate('Member');
    setVisible(false);
  };
  return (
    <Menu
      visible={visible}
      anchor={
        <Icon
          type="feather"
          name="menu"
          size={25}
          color="#000"
          onPress={() => setVisible(!visible)}
        />
      }
      onDismiss={() => setVisible(false)}>
      <Menu.Item title="Member" onPress={handleNavigate} />
    </Menu>
  );
};

const MyLeftComponent = props => {
  return (
    <Icon
      type="feather"
      name="arrow-left"
      size={24}
      containerStyle={styles.icon}
      onPress={props.back}
    />
  );
};

const Payment = () => {

  const idproject = useSelector(state => state.project.idProject);
  const card = useSelector(state => state.project.cardProject);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const loading = useSelector(state => state.project.isLoading);
  const title = useSelector(state => state.project.title);
  const [swipe, setSwipe] = useState(true);
  const [drag, setDrag] = useState(false);

  useEffect(() => {
    dispatch(getProjectDetail(idproject));
  }, [dispatch, idproject]);

  const handleDetailTaskNavigator = id => {
    dispatch({type: GET_TASK_ID, idTask: id});
    navigation.navigate('Task Detail');
  };

  let matches;
  let initial;

  if (title !== null) {
    matches = title.match(/\b(\w)/g);
    initial = matches.join('');
  };
  const renderItem = ({item}) => (
    <View style={styles.cardProject}>
      <StatusBar backgroundColor="#FFF" barStyle="dark-content" />
      <View style={styles.cardHeaderContainer}>
        <Text style={styles.header}>{item.name}</Text>
      </View>
      </View>
  )
    return (
    <View>
    <Text style={{fontSize:50, marginTop: 20, textAlign:'center'}}>Payment</Text>
    <Board
    style={styles.board}
    rowRepository={this.state.boardRepository}
    renderRow={this.renderRow.bind(this)}
    renderColumnWrapper={this.renderColumnWrapper.bind(this)}
    open={this.onOpen.bind(this)}
    onDragEnd={this.onDragEnd.bind(this)}
  />
  />
    {/* // const navigation = useNavigation(); */}
    </View>
    )};
    const data = [
      {    
        id: {data},
        name: 'BACKLOG',
        rows: [
          {
            id : '1',
            name: '(item.tittle)',
            description: '(item.description)'
          },
        ]
      },
      {
        id: 2,
        name: 'TO DO',
        rows: [
          {
            id: '4',
            name: 'Look at drawings',
            description: 'How did they use line and shape? How did they shade?'
          },
        ]
      },
      {
        id: 3,
        name: 'IN PROGRESS',
        rows: [
          {
            id: '7',
            name: 'Draw from life',
            description: 'Do you enjoy coffee? Draw your coffee cup'
          },
        ]
      },
      {
      id: 4,
        name: 'DONE',
        rows: [
          {
            id: '9',
            name: 'HUHU',
            description: 'Do you enjoy coffee? Draw your coffee cup'
          },
        ]
      }
    ]
    const boardRepository = new BoardRepository(data);
          boardRepository.updateData(data);
export default Payment;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})