import {lightTheme} from '../../theme';
import {SWITCH_THEME} from '../Action/themeAction';

const initialState = {
    theme: lightTheme
}

const themeReducer = (state = initialState, action) => {
    switch(action.type) {
        case SWITCH_THEME:
            return{theme: Action.theme}

        default:
            return state;
    }
}

export default themeReducer;