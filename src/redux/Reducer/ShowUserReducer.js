import {
  GET_EMAIL,
  GET_NAME,
  GET_PROFILE_IMAGE,
  GET_BIO,
  GET_PROFILE_ID,
} from '../Case Type/Showuser';

const initialState = {
  email: null,
  name: null,
  bio: null,
  idProfile: null,
  imageProfile: null,
  history: [],
};

export const ShowUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_EMAIL:
      return {
        ...state,
        email: action.email,
      };
    case GET_NAME:
      return {
        ...state,
        name: action.name,
      };
    case GET_BIO:
      return {
        ...state,
        bio: action.bio,
      };
    case GET_PROFILE_ID:
      return {
        ...state,
        idProfile: action.idProfile,
      };
    case GET_PROFILE_IMAGE:
      return {
        ...state,
        imageProfile: action.imageProfile,
      };
    case 'GET_HISTORY':
      return {
        ...state,
        history: action.history,
      };
    default:
      return state;
  }
};
