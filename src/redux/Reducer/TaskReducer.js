import {
  GET_TASK_ID,
  GET_TASK_TITLE,
  GET_TASK_DESCRIPTION,
  GET_TASK_CARD_ID,
  GET_TASK_IMAGE,
  GET_TASK_ALL,
  TASK_LOADING,
  TASK_STATUS,
  TASK_ASSIGNMENT,
  TASK_ALERT,
  TASK_MESSAGE,
} from '../Case Type/Task';

const initialState = {
  idTask: null,
  title: null,
  description: null,
  image: [],
  cardId: null,
  status: null,
  isLoading: false,
  tasks: [],
  assignment: [],
  alert: false,
  message: null,
  moving: null,
};

export const TaskReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TASK_ID:
      return {
        ...state,
        idTask: action.idTask,
      };
    case GET_TASK_TITLE:
      return {
        ...state,
        title: action.title,
      };
    case GET_TASK_DESCRIPTION:
      return {
        ...state,
        description: action.description,
      };
    case GET_TASK_CARD_ID:
      return {
        ...state,
        cardId: action.cardId,
      };
    case GET_TASK_IMAGE:
      return {
        ...state,
        image: action.image,
      };
    case TASK_STATUS:
      return {
        ...state,
        status: action.status,
      };
    case TASK_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case GET_TASK_ALL:
      return {
        ...state,
        tasks: action.tasks,
      };
    case TASK_ASSIGNMENT:
      return {
        ...state,
        assignment: action.assignment,
      };
    case TASK_ALERT:
      return {
        ...state,
        alert: false,
      };
    case TASK_MESSAGE:
      return {
        ...state,
        message: action.message,
        alert: true,
      };
    case 'MOVING':
      return {
        ...state,
        moving: action.moving,
      };
    default:
      return state;
  }
};
