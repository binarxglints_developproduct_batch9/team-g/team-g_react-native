import {
  GET_PROJECT_ID,
  GET_PROJECT_TITLE,
  GET_PROJECT_DESCRIPTION,
  GET_PROJECT_IMAGE,
  GET_PROJECT_CARD,
  GET_PROJECT_MEMBER,
  GET_PROJECT_ALL,
  STATUS,
  LOADING,
  MESSAGE,
  ALERT,
} from '../Case Type/Project';

const initialState = {
  idProject: null,
  title: null,
  description: null,
  imageProject: null,
  cardProject: [],
  projectMember: [],
  project: [],
  status: null,
  isLoading: false,
  message: null,
  alert: false,
  total_task: null,
};

export const ProjectReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROJECT_ID:
      return {
        ...state,
        idProject: action.idProject,
      };
    case GET_PROJECT_TITLE:
      return {
        ...state,
        title: action.title,
      };
    case GET_PROJECT_DESCRIPTION:
      return {
        ...state,
        description: action.description,
      };
    case GET_PROJECT_IMAGE:
      return {
        ...state,
        imageProject: action.imageProject,
      };
    case GET_PROJECT_CARD:
      return {
        ...state,
        cardProject: action.cardProject,
      };
    case GET_PROJECT_MEMBER:
      return {
        ...state,
        projectMember: action.projectMember,
      };
    case GET_PROJECT_ALL:
      return {
        ...state,
        project: action.project,
        total_task: action.task,
      };
    case STATUS:
      return {
        ...state,
        status: action.status,
      };
    case LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case MESSAGE:
      return {
        ...state,
        message: action.message,
        alert: true,
      };
    case ALERT:
      return {
        ...state,
        alert: false,
      };
    default:
      return state;
  }
};
