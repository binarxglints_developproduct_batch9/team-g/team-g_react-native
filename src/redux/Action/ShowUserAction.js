import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import { API_ACTIVITY, API_USER } from '../../API';
import {
  GET_PROFILE_ID,
  GET_PROFILE_IMAGE,
  GET_EMAIL,
  GET_NAME,
  GET_BIO,
} from '../Case Type/Showuser';

export const getUser = () => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      Axios({
        method: 'GET',
        url: `${API_USER}/get`,
        headers: {
          Authorization: `bearer ${token}`
        }
      })
        .then(({ data }) => {
          if (data !== null) {

            dispatch({
              type: GET_PROFILE_IMAGE,
              imageProfile: data.data.image,
            });
            dispatch({ type: GET_EMAIL, email: data.data.email });
            dispatch({ type: GET_NAME, name: data.data.fullName });
            dispatch({ type: GET_BIO, bio: data.data.bio });
          }
        })
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const getNews = () => {
  return async dispatch => {
    try {
      const Axios = require('axios')
      const token = await AsyncStorage.getItem('userToken');
      Axios({
        method: 'GET',
        url: `${API_ACTIVITY}/useractivity`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then(({ data }) => {
          if (data !== null) {
            // const data = data.countData.countTask;
            dispatch({
              type: 'GET_HISTORY',
              history: data.countData.countTask,
            })
          }
        })
      // })
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
