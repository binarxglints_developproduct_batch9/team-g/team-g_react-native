import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import { API_PROJECT } from '../../API';
import { API_TASKS } from '../../API';
import {
  GET_PROJECT_ID,
  GET_PROJECT_ALL,
  GET_PROJECT_TITLE,
  GET_PROJECT_IMAGE,
  GET_PROJECT_CARD,
  GET_PROJECT_DESCRIPTION,
  STATUS,
  LOADING,
  GET_PROJECT_MEMBER,
  MESSAGE,
} from '../Case Type/Project';

export const getAllProject = () => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      Axios({
        method: 'GET',
        url: `${API_PROJECT}/user`,
        headers: {
          Authorization: `bearer ${token}`,
        }
      })
        .then(({ data }) => {
          if (data !== null) {
            dispatch({
              type: GET_PROJECT_ALL,
              project: data.data,
              task: data.total_task,
            });
          }
        })
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
export const getOne = () => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      console.log("ini id project " + project)
      Axios({
        method: 'GET',
        url: `${API_PROJECT}/getOne/${idProject}`,
        headers: {
          Authorization: `bearer ${token}`
        }
      })
        .then(({ data }) => {
          if (data !== null) {
            dispatch({
              name: data.name,
              description: data.description,
              bgImage: data.bgImage
            })
          }
        })
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
export const getProjectDetail = _id => {
  return async dispatch => {
    dispatch({ type: LOADING, isLoading: true });
    try {
      const token = await AsyncStorage.getItem('userToken');
      // const res = await Axios.get(`${API_PROJECT}/showOne/${id}`, {
      //   headers: {
      //     auth_token: token,
      //   },
      // });
      // var boardRepository
      Axios({
        method: 'GET',
        url: `${API_TASKS}/getAllTask/${idproject}`,
        headers: {
          Authorization: `bearer ${token}`
        }
      })
        .then(({ data }) => {

          // if (data !== null) {
          // const data = res.data.data;
          var DataBacklog = data.data['600fc864999f0f2266cebed2'];
          var Backlogrow = DataBacklog.tasks['600fcb6c999f0f2266cebedc'];
          var brow = new cRows(Backlogrow.position, Backlogrow.name, Backlogrow.description);
          var arrxrow = [brow]
          var sboard = new cBoard(DataBacklog.idProject, DataBacklog.name, arrxrow)
          var dnd = [sboard]
          boardRepository = new BoardRepository(dnd)
          // dispatch({
          //   type: GET_PROJECT_TITLE,
          //   title: data.dataProject.name,
          // });
          // dispatch({
          //   type: GET_PROJECT_DESCRIPTION,
          //   description: data.dataProject.description,
          // });
          // dispatch({
          //   type: GET_PROJECT_IMAGE,
          //   imageProject: data.dataProject.bgImage,
          // });
          // dispatch({
          //   type: GET_PROJECT_CARD,
          //   cardProject: data.dataProject.card,
          // });
          // dispatch({ type: LOADING, isLoading: false });
          // }
        }
        )
    } catch (error) {
      console.log(error, 'error');
      dispatch({ type: LOADING, isLoading: false });
    }
    class cBoard {
      constructor(id, name, rows) {
        this.id = id;
        this.name = name;
        this.rows = rows;
      }
    }
    class cRows {
      constructor(id, name, description) {
        this.id = id;
        this.name = name;
        this.description = description;
      }
    }
  };
};

export const createProject = (dataForm, navigation) => {
  return async dispatch => {
    dispatch({ type: LOADING, isLoading: true });
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.post(API_PROJECT + '/create',
        dataForm,
        {
          headers: {
            Authorization: `bearer ${token}`
          }
        });

      if (res !== null) {
        const status = res.data.status;
        dispatch({ type: STATUS, status: status });
        dispatch({ type: LOADING, isLoading: false });
        if (status == 'success') {
          navigation.navigate('Project');
        }

      }
    } catch (error) {
      console.log('error', error);
      dispatch({ type: LOADING, isLoading: false });
    }
  };
};

export const deleteProject = _id => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      Axios({
        method: 'DELETE',
        url: `${API_PROJECT}/delete/${_id}`,
        headers: {
          Authorization: `bearer ${token}`
        }
      })
        .then(({ data }) => {
          if (data !== null) {
            console.log('Project deleted');
          }
        })
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const editProject = (_id, dataForm, navigation) => {
  return async dispatch => {
    dispatch({ type: LOADING, isLoading: true });
    try {
      const token = await AsyncStorage.getItem('userToken');
      // const res = await Axios.put(API_PROJECT+`/update/${_id}`, dataForm, {
      Axios({
        method: 'PUT',
        url: `${API_PROJECT}/update/${_id}`,
        dataForm,
        headers: {
          Authorization: `bearer ${token}`
        }
      })
        .then(({ data }) => {
          if (data !== null) {
            const status = data.status;
            console.log("ini status "+ status)
            dispatch({ type: STATUS, status: status });
            dispatch({ type: LOADING, isLoading: false });
            if (status == 'success') {
              navigation.navigate('Project');
            }
          }
        })
    } catch (error) {
      console.log(error, 'error');
      dispatch({ type: LOADING, isLoading: false });
    }
  };
};

export const inviteMember = (id, member) => {
  return async dispatch => {
    try {
      dispatch({ type: LOADING, isLoading: true });
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.post(
        `${API_PROJECT}/member/invite/${id}`,
        {
          email: member,
        },
        {
          headers: {
            auth_token: token,
          },
        },
      );

      if (res !== null) {
        console.log(res.data);
        dispatch({ type: LOADING, isLoading: false });
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({ type: LOADING, isLoading: false });
      dispatch({ type: MESSAGE, message: error.response.data.message });
    }
  };
};

export const deleteMember = id => {
  return async dispatch => {
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.delete(`${API_PROJECT}/member/kick/${id}`, {
        headers: {
          auth_token: token,
        },
      });

      if (res !== null) {
        console.log(res.data);
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};

export const getMember = _id => {
  return async dispatch => {
    try {
      console.log(idProject)
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.get(`${API_PROJECT}/member/${_id}`, {
        headers: {
          Authorization: `bearer ${token}`,
        },
      });
      if (res !== null) {
        const data = res.data.members;
        dispatch({ type: GET_PROJECT_MEMBER, projectMember: data });
      }
    } catch (error) {
      console.log(error, 'error');
    }
  };
};
