import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LandingScreen from './screen/landing screen/LandingScreen';
import SplashScreen from './screen/Splash Screen/SplashScreen';
import Login from './screen/login screen/Login';
import Signup from './screen/signup screen/Signup';
import TaskDetail from './screen/task screen/TaskDetail';
import { StyleSheet } from 'react-native';
import RootTab from './RootTabNavigator';
import EditProfile from './screen/edit profile screen/EditProfile';
import CreateProject from './screen/create project screen/CreateProject';
import EditProject from './screen/edit project screen/EditProject';
import CreateTask from './screen/task screen/CreateTask';
import EditTask from './screen/task screen/EditTask';
import UpdatePassword from './screen/update password screen/UpdatePassword';
import Member from './screen/member screen/Member';
import ProjectDetail from './screen/dashboard/project screen/ProjectDetail';
import Verify from './verify screen/Verify';
import Payment from './Payment/payment';
import DetailProject from './Payment/Detailproject';
import Midtrans from './Payment/Midtrans'

const Stack = createStackNavigator();
const RootStack = () => {
  return (
    <Stack.Navigator initialRouteName="Splash Screen" headerMode="screen">
      <Stack.Screen
        name="Landing Page"
        component={LandingScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Splash Screen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Signup"
        component={Signup}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Task Detail"
        component={TaskDetail}
        options={{
          // headerRight: MyRightComponent,
          headerTitle: 'Task Detail',
          headerTitleStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="Dashboard"
        component={RootTab}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Edit Profile"
        component={EditProfile}
        options={{
          headerShown: true,
          headerTitle: 'Edit Profile',
          headerTitleStyle: styles.Payheader,
        }}
      />
      <Stack.Screen
        name="Create Project"
        component={CreateProject}
        options={{
          headerShown: true,
          headerTitle: 'Create New Project',
          headerTitleStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="Create Task"
        component={CreateTask}
        options={{
          headerShown: true,
          headerTitle: 'Create New Task',
          headerTitleStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="Edit Task"
        component={EditTask}
        options={{
          headerShown: true,
          headerTitle: 'Edit Task',
          headerTitleStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="Edit Project"
        component={EditProject}
        options={{
          headerShown: true,
          headerTitle: 'Edit Project',
          headerTitleStyle: styles.headeredit,
        }}
      />
      <Stack.Screen
        name="Update Password"
        component={UpdatePassword}
        options={{
          headerShown: true,
          headerTitle: 'Change Password',
          headerTitleStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="Member"
        component={Member}
        options={{
          headerShown: false,
          headerTitle: 'Members',
          headerTitleStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="Project Detail"
        component={ProjectDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Verify"
        component={Verify}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Payment"
        component={Payment}
        options={{ headerShown: true,
                  headerTitleStyle: styles.Payheader}}
      />
      <Stack.Screen
        name="Detail Project"
        component={DetailProject}
        options={{ headerShown: false,
                    headerTitleStyle: styles.Payheader}}
      />
      <Stack.Screen
        name="Midtrans"
        component={Midtrans}
        options={{ headerShown: false,
                    headerTitleStyle: styles.Payheader}}
      />
    </Stack.Navigator>
  );
};
export default RootStack;

const styles = StyleSheet.create({
  header: {
    height: 120,
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginTop: 95,
    marginLeft:55,
    color: '#000'
  },
  headeredit: {
    height: 100,
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginTop: 75,
    color: '#000'
  },
  Payheader: {
    height: 120,
    fontFamily: 'Montserrat',
    fontWeight: '500',
    fontSize: 18,
    marginTop: 95,
    marginHorizontal:80,
    color: '#000'
  },
});
