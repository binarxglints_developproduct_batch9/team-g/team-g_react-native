import React, {useEffect} from 'react';
import DashboardHome from './screen/dashboard/DashboardHome';
import {Icon, Image} from 'react-native-elements';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {StyleSheet} from 'react-native';
import Avatar from './asset/avatar.png';
import DashboardProfile from './screen/dashboard/DashboardProfile';
import {useSelector, useDispatch} from 'react-redux';
import {getUser} from './redux/Action/ShowUserAction';
import DashboardProject from './screen/dashboard/project screen/DashboardProject';

const Tab = createBottomTabNavigator();

const RootTab = () => {
  const profile = useSelector(state => state.showUser);
  const dispatch = useDispatch();
  const image = profile.imageProfile;

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);
  let avatar;

  if (image == null) {
    avatar = Avatar;
  } else {
    avatar = {uri: image};
  }

  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeBackgroundColor: 'rgba(21,137,203, 0.8)',
        tabStyle: styles.tabItem,
        style: styles.tabContainer,
      }}
      initialRouteName="Home">
      <Tab.Screen
        name="Home"
        component={DashboardHome}
        options={{
          title: 'Dashboard',
          tabBarIcon: ({focused}) => {
            return (
              <Icon
                type="feather"
                name="layout"
                size={25}
                color={focused ? '#FFF' : '#7B7B7B'}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Project"
        component={DashboardProject}
        options={{
          title: 'Project',
          tabBarIcon: ({focused}) => {
            return (
              <Icon
                type="feather"
                name="file-text"
                size={25}
                color={focused ? '#FFFF' : '#7B7B7B'}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={DashboardProfile}
        size={25}
        options={{
          title: 'Profile',
          tabBarIcon: ({}) => {
            return <Image source={avatar} style={styles.profileImage} />;
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default RootTab;

const styles = StyleSheet.create({
  tabContainer: {
    justifyContent: 'center',
    height: 70,
    alignItems: 'center',
    backgroundColor:'#FAFAFA',
    borderTopWidth:1,
    borderTopColor:'#000'
  },
  tabItem: {
    alignSelf: 'center',
    margin: 45,
    height: 40,
    borderRadius:3,
  },
  profileImage: {
    width: 30,
    height: 30,
    borderRadius: 100,
  },
});
