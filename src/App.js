import React from 'react';
import AppNavigator from './AppNavigator';
import {Provider} from 'react-redux';
import {store} from './redux/store';
import {Provider as PaperProvider} from 'react-native-paper';
import messaging from '@react-native-firebase/messaging';
import themeReducer from './redux/Reducer/themeReducer';
messaging().setBackgroundMessageHandler(mess => {
  console.log('message', mess);
});

const App = () => {
  return (
    <Provider store={store}>
      <PaperProvider>
        <AppNavigator />
      </PaperProvider>
    </Provider>
  );
};

export default App;
