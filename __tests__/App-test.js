/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../src/App';
import ShowUserAction from '../src/redux/Action/ShowUserAction'

import { render, fireEvent } from '@testing-library/react-native';


jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

it('renders correctly', async () => {
  const component = (<ShowUserAction />)
  const { findByText, findAllByText } = render(component);
  const user = await findAllByText(/user/i)
  expect(user).toBeTruthy()
});

